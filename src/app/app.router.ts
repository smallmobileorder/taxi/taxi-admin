import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {LoginComponent} from './components/login/login.component';
import {Auth_authorizationGuard} from './auth_authorization.guard';
import {DriversResolver} from './resolvers/drivers-resolver.service';
import {DriversComponent} from './components/drivers/drivers.component';
import {ClientsComponent} from './components/clients/clients.component';
import {TransactionsComponent} from './components/transactions/transactions.component';
import {ModerationComponent} from './components/moderation/moderation.component';
import {CompleteOrderComponent} from './components/complete-order/complete-order.component';
import {ActiveOrderComponent} from './components/active-order/active-order.component';
import {MainConfigComponent} from './components/main-config/main-config.component';
import {RegistrationDriverComponent} from './components/registration-internal/registration-driver.component';
import {MainComponent} from './components/main/main.component';
import {AuthGuard1} from './auth_deauthorization.guard';
import {SupportComponent} from './components/support/support.component';
import {ModerationApproveComponent} from './components/moderation-approve/moderation-approve.component';
import {ClientsResolver} from './resolvers/clients-resolver.service';
import {TransactionsResolver} from './resolvers/transaction-resolver.service';
import {AccountApplicationResolver} from './resolvers/account-application-resolver.service';
import {ModerationResolver} from './resolvers/moderation-resolver.service';
import {MainConfigResolver} from './resolvers/main-config-resolver.service';
import {AddTransactionComponent} from './components/add-transaction/add-transaction.component';
import {OrderInfoComponent} from './components/order-info/order-info.component';
import {ActiveOrdersResolver} from './resolvers/active-orders-resolver.service';
import {FinishedOrdersResolver} from './resolvers/finished-orders-resolver.service';
import {OrderResolver} from './resolvers/info-order-resolver.service';
import {BadRequestComponent} from './components/bad-request/bad-request.component';
import {TariffsConfigComponent} from './components/tariffs-config/tariffs-config.component';
import {TariffsConfigService} from './services/tariffs-config.service';
import {TariffsResolverService} from './resolvers/tariffs-resolver.service';
import {LiveDriversComponent} from './components/live-drivers/live-drivers.component';
import {LiveDriversResolver} from './resolvers/live-drivers-resolver.service';

export const MAIN_URL = 'https://ec2-3-136-159-160.us-east-2.compute.amazonaws.com';
// export const MAIN_URL = 'https://192.168.0.104';

const itemRoutes: Routes = [
  {
    path: 'database/drivers',
    component: DriversComponent,
    canActivate: [Auth_authorizationGuard],
    resolve: {data: DriversResolver},
    pathMatch: 'full'
  },
  {
    path: 'database/clients',
    component: ClientsComponent,
    canActivate: [Auth_authorizationGuard],
    resolve: {data: ClientsResolver},
    pathMatch: 'full'
  },
  {
    path: 'database/transactions', component: TransactionsComponent, canActivate: [Auth_authorizationGuard],
    resolve: {data: TransactionsResolver}, pathMatch: 'full'
  },
  {
    path: 'drivers/moderation',
    component: ModerationComponent,
    canActivate: [Auth_authorizationGuard],
    resolve: {data: ModerationResolver},
    pathMatch: 'full'
  },
  {
    path: 'call-center/working/complete-order',
    component: CompleteOrderComponent,
    canActivate: [Auth_authorizationGuard],
    pathMatch: 'full',
    resolve: {data: FinishedOrdersResolver}
  },
  {
    path: 'call-center/working/order-info/:id',
    component: OrderInfoComponent,
    canActivate: [Auth_authorizationGuard],
    pathMatch: 'full',
    resolve: {data: OrderResolver}
  },
  {
    path: 'database/add-transaction',
    component: AddTransactionComponent,
    canActivate: [Auth_authorizationGuard],
    pathMatch: 'full',
    resolve: {data: DriversResolver}
  },
  {
    path: 'call-center/working/active-order',
    component: ActiveOrderComponent,
    canActivate: [Auth_authorizationGuard],
    pathMatch: 'full',
    resolve: {data: ActiveOrdersResolver}
  },
  {
    path: 'call-center/working/live-driver',
    component: LiveDriversComponent,
    canActivate: [Auth_authorizationGuard],
    resolve: {data: LiveDriversResolver},
    pathMatch: 'full'
  },
  {
    path: 'config/main-config',
    component: MainConfigComponent,
    canActivate: [Auth_authorizationGuard],
    resolve: {data: MainConfigResolver}
  },
  {
    path: 'config/tariffs-config',
    component: TariffsConfigComponent,
    canActivate: [Auth_authorizationGuard],
    resolve: {data: TariffsResolverService}
  },
  {
    path: 'drivers/registration-internal',
    component: RegistrationDriverComponent,
    canActivate: [Auth_authorizationGuard],
  },
  {
    path: 'drivers/approved/:id',
    component: ModerationApproveComponent,
    canActivate: [Auth_authorizationGuard],
    resolve: {data: AccountApplicationResolver}
  },
  {
    path: 'call-center/working/support',
    component: SupportComponent,
    canActivate: [Auth_authorizationGuard],
  }
];

export const routes: Routes = [
  {
    path: '',
    canActivate: [Auth_authorizationGuard],
    redirectTo: 'main/drivers/registration-internal',
    pathMatch: 'full'
  },
  {path: 'login', component: LoginComponent, pathMatch: 'full', canActivate: [AuthGuard1]},
  {path: 'main', component: MainComponent, children: itemRoutes},
  {path: 'bad-request', component: BadRequestComponent},
  {path: '**', canActivate: [Auth_authorizationGuard], redirectTo: 'main/drivers/registration-internal'}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
