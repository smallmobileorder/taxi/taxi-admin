import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MainComponent} from '../main/main.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {DateAdapter, MAT_DATE_LOCALE} from '@angular/material/core';
import {OrderService} from '../../services/order.service';
import {OrderInfo} from '../../models/OrderActive';

@Component({
  selector: 'app-order-info',
  templateUrl: './order-info.component.html',
  styleUrls: ['./order-info.component.scss'],
  providers: [{provide: MAT_DATE_LOCALE, useValue: 'ru'}]
})
export class OrderInfoComponent implements OnInit {
  phoneClientForm = new FormControl('', [Validators.required]);
  phoneDriverForm = new FormControl('', [Validators.required]);
  fioClientForm = new FormControl('', [Validators.required]);
  fioDriverForm = new FormControl('', [Validators.required]);
  addressDepartForm = new FormControl('', [Validators.required]);
  addressDestForm = new FormControl('', [Validators.required]);
  sumForm = new FormControl('', [Validators.required]);
  phoneClient = '';
  phoneDriver = '';
  fioClient = '';
  fioDriver = '';
  addressDepart = '';
  addressDest = '';
  sum = '';
  private idOrder = '0';
  private data: any;
  private orderInfo: OrderInfo;
  dateCreate = '';
  dateFinish = '';

  constructor(private mainComponent: MainComponent, private snackBar: MatSnackBar,
              private orderService: OrderService, private router: Router,
              private activateRoute: ActivatedRoute, private adapter: DateAdapter<any>) {
    adapter.setLocale('ru');
    activateRoute.params.subscribe(params => this.idOrder = params.id);
    this.data = this.activateRoute.snapshot.data.data;
    console.log(this.data);
  }

  ngOnInit() {
    this.orderInfo = this.orderService.getOrderInfo(this.data);
    console.log(this.orderInfo);
    this.fillData(this.orderInfo);
  }

  private fillData(orderInfo: OrderInfo) {
    this.dateCreate = orderInfo.datetime_start;
    this.dateFinish = orderInfo.datetime_finish;
    this.sum = orderInfo.sum;
    this.addressDepart = orderInfo.address_depart;
    this.addressDest = orderInfo.address_dest;
    this.fioClient = orderInfo.fio_client;
    this.fioDriver = orderInfo.fio_driver;
    this.phoneDriver = orderInfo.phone_driver;
    this.phoneClient = orderInfo.phone_client;
    this.addressDest = orderInfo.address_dest;
  }
}
