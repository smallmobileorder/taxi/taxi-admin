import {AfterViewInit, Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {GoogleMapsService} from 'google-maps-angular2';
import {MainComponent} from '../main/main.component';
import {DriversService} from '../../services/drivers.service';
import {ActivatedRoute, Router} from '@angular/router';
import {LiveDriversService} from '../../services/live-drivers.service';
import {DriverLatLng, DriverTable} from '../../models/driver';
import {Observable, of, Subscription} from 'rxjs';
import {delay} from 'rxjs/operators';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-live-drivers',
  templateUrl: './live-drivers.component.html',
  styleUrls: ['./live-drivers.component.scss']
})
export class LiveDriversComponent implements OnInit {
  constructor(private bottomSheet: MatBottomSheet, private mainComponent: MainComponent, private driverService: DriversService, private liveDriverService: LiveDriversService,
              private route: ActivatedRoute) {
    this.data = this.route.snapshot.data.data;
  }


  private driversWtihLatLng: DriverLatLng[];

  private data: any;

  latitude = 59.94;
  longitude = 30.31;
  mapType = 'satellite';

  selectedMarker;
  drivers = [];
  public darkStyle = [
    {
      elementType: 'geometry',
      stylers: [
        {
          hue: '#ff4400'
        },
        {
          saturation: -100
        },
        {
          lightness: -4
        },
        {
          gamma: 0.72
        }
      ]
    },
    {
      featureType: 'road',
      elementType: 'labels.icon'
    },
    {
      featureType: 'landscape.man_made',
      elementType: 'geometry',
      stylers: [
        {
          hue: '#0077ff'
        },
        {
          gamma: 3.1
        }
      ]
    },
    {
      featureType: 'water',
      stylers: [
        {
          hue: '#000000'
        },
        {
          gamma: 0.44
        },
        {
          saturation: -33
        }
      ]
    },
    {
      featureType: 'poi.park',
      stylers: [
        {
          hue: '#44ff00'
        },
        {
          saturation: -23
        }
      ]
    },
    {
      featureType: 'water',
      elementType: 'labels.text.fill',
      stylers: [
        {
          hue: '#007fff'
        },
        {
          gamma: 0.77
        },
        {
          saturation: 65
        },
        {
          lightness: 99
        }
      ]
    },
    {
      featureType: 'water',
      elementType: 'labels.text.stroke',
      stylers: [
        {
          gamma: 0.11
        },
        {
          weight: 5.6
        },
        {
          saturation: 99
        },
        {
          hue: '#0091ff'
        },
        {
          lightness: -86
        }
      ]
    },
    {
      featureType: 'transit.line',
      elementType: 'geometry',
      stylers: [
        {
          lightness: -48
        },
        {
          hue: '#ff5e00'
        },
        {
          gamma: 1.2
        },
        {
          saturation: -23
        }
      ]
    },
    {
      featureType: 'transit',
      elementType: 'labels.text.stroke',
      stylers: [
        {
          saturation: -64
        },
        {
          hue: '#ff9100'
        },
        {
          lightness: 16
        },
        {
          gamma: 0.47
        },
        {
          weight: 2.7
        }
      ]
    },
    {
      featureType: 'water',
      elementType: 'geometry',
      stylers: [
        {
          color: '#777777'
        }
      ]
    },
  ];

  ngOnInit(): void {
    this.mainComponent.initActiveNode('Водители', 'main/database/drivers');
    console.log(this.data);
    this.refreshDrivers(this.data);
    this.runnableFun();
  }

  runnableFun() {
    of([1, 2, 3]).pipe(delay(5000)).subscribe(item1 => {
        this.liveDriverService.getDriverLatLngObservable().subscribe(it => {
          console.log(it);
          this.refreshDrivers(it);
        });
        this.runnableFun();
      },
      error1 => {
        this.runnableFun();
      }
    )
    ;
  }

  refreshDrivers(data: any) {
    this.driversWtihLatLng = this.liveDriverService.getDriversWithLatLng(data);
    console.log(this.driversWtihLatLng);
    this.drivers = [];
    this.driversWtihLatLng.forEach(item => {
      this.drivers.push({
        id: item.id,
        lat: item.lat,
        lng: item.lng,
        last_angel: item.last_angel,
        phone_number: item.phone_number,
        first_name: item.first_name,
        last_name: item.last_name,
        patronymic: item.patronymic,
        alpha: 1,
        icon: 'assets/car.ico'
      });
      console.log(this.drivers[0].last_angel);
    });
  }

  openCar(driver: DriverLatLng) {
    this.bottomSheet.open(BottomSheetComponent, {
      data: {
        id_dr: driver.id,
        phone_number: driver.phone_number,
        first_name: driver.first_name,
        last_name: driver.last_name,
        patronymic: driver.patronymic
      }
    });
  }
}

@Component({
  selector: 'app-bottom-sheet',
  templateUrl: 'bottom-sheet.html',
})
export class BottomSheetComponent implements OnInit{
  firstName: string;
  lastName: string;
  patronymic: string;
  phoneNumber: string;
  constructor(private router: Router, private bottomSheetRef: MatBottomSheetRef<BottomSheetComponent>, @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {
  }

  openDriver() {
    this.bottomSheetRef.dismiss();
    this.router.navigate(['main/drivers/approved/' + this.data.id_dr]);
  }

  openOrder() {
    this.bottomSheetRef.dismiss();
    this.router.navigate(['main/drivers/approved/' + this.data.id_dr]);
  }

  ngOnInit(): void {
    this.firstName = this.data.first_name;
    this.lastName = this.data.last_name;
    this.patronymic = this.data.patronymic;
    this.phoneNumber = this.data.phone_number;
  }
}
