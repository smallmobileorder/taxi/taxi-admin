import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {DatePipe} from '@angular/common';
import {
  ErrorStateMatcher,
  MAT_DIALOG_DATA,
  MAT_RADIO_DEFAULT_OPTIONS,
  MatDialog,
  MatDialogRef,
  MatRadioButton,
  MatSnackBar
} from '@angular/material';
import {MainComponent} from '../main/main.component';
import {ActivatedRoute, Router} from '@angular/router';
import {AccountApplicationService} from '../../services/account-application.service';
import {Subscription} from 'rxjs';
import {AccountApplication} from '../../models/accountApplication';
import {DateAdapter, MAT_DATE_LOCALE} from '@angular/material/core';

@Component({
  selector: 'app-moderation-approve',
  templateUrl: './moderation-approve.component.html',
  styleUrls: ['./moderation-approve.component.scss'],
  providers: [DatePipe, {
    provide: MAT_RADIO_DEFAULT_OPTIONS,
    useValue: {color: 'primary'}
  }, {provide: MAT_DATE_LOCALE, useValue: 'ru'}],

})
export class ModerationApproveComponent implements OnInit {
  durationInSeconds = 5;
  @ViewChild('rad1', {static: false}) rad1: MatRadioButton;
  @ViewChild('rad2', {static: false}) rad2: MatRadioButton;
  private idDriver: string;
  private subscription: Subscription;
  private accountApplication: AccountApplication;
  private data: any;

  constructor(private mainComponent: MainComponent, public dialog: MatDialog, private snackBar: MatSnackBar,
              private accountApplicationService: AccountApplicationService, private router: Router,
              private activateRoute: ActivatedRoute, private adapter: DateAdapter<any>) {
    adapter.setLocale('ru');
    this.subscription = activateRoute.params.subscribe(params => this.idDriver = params.id);
    this.data = this.activateRoute.snapshot.data.data;
  }

  expand1 = false;
  expand2 = false;
  expand3 = false;
  birthdayValue = '';
  addressValue = '';
  addressValue2 = '';
  phoneValue = '';
  emailValue = '';
  seriaPasportValue = '';
  numberPasportValue = '';
  datePasportValue = '';
  seriaVodValue = '';
  numberVodValue = '';
  dateVodValue = '';
  dateAccessValue = '';
  markValue = '';
  modelValue = '';
  colorValue = '';
  numberValue = '';
  yearValue = '';
  countDoorValue = '';
  vinValue = '';
  commentValue = '';
  dateWritingValue = '';
  timeWritingValue = '';
  firstNameValue = '';
  secondNameValue = '';
  patronymicNameValue = '';
  status = 'ACCEPTED';

  ngOnInit() {
    this.accountApplication = this.accountApplicationService.getAccountApplicationData(this.data);
    this.fillComponents(this.accountApplication);
    this.mainComponent.initActiveNode('Прием заявки', 'main/drivers/approve');
    this.expand1 = !this.expand1;
  }

  private fillComponents(accountApplication: AccountApplication) {
    this.firstNameValue = accountApplication.first_name;
    this.secondNameValue = accountApplication.second_name;
    this.patronymicNameValue = accountApplication.patronymic;
    this.birthdayValue = new Date(accountApplication.date_birthday).toLocaleDateString();
    this.addressValue = accountApplication.address1;
    this.addressValue2 = accountApplication.address2;
    this.phoneValue = accountApplication.phone;
    this.emailValue = accountApplication.email;
    this.seriaPasportValue = accountApplication.seria_pasport;
    this.numberPasportValue = accountApplication.number_pasport;
    this.datePasportValue = new Date(accountApplication.date_vidachi_pasport).toLocaleDateString();
    this.seriaVodValue = accountApplication.seria_voditelskoye;
    this.numberVodValue = accountApplication.number_voditelskoye;
    this.dateVodValue = new Date(accountApplication.date_vidachi_voditelskoye).toLocaleDateString();
    this.dateAccessValue = new Date(accountApplication.date_access_voditelskoye).toLocaleDateString();
    this.markValue = accountApplication.mark;
    this.modelValue = accountApplication.model;
    this.colorValue = accountApplication.color;
    this.numberValue = accountApplication.number;
    this.yearValue = accountApplication.year_making;
    this.countDoorValue = accountApplication.count_of_doors;
    this.vinValue = accountApplication.vin;
    this.commentValue = accountApplication.comment;
    const writeTimestamp = new Date(accountApplication.date_writing);
    this.status = accountApplication.status;
    this.dateWritingValue = writeTimestamp.toLocaleDateString();
    this.timeWritingValue = writeTimestamp.getHours() + ':' + writeTimestamp.getMinutes();

  }

  openSnackBarAccept(str: string) {
    this.snackBar.open(str, '', {
      duration: this.durationInSeconds * 1000,
      panelClass: ['snackbar-container']
    });
  }

  openDialog(state: number) {
    const dialogRef = this.dialog.open(DialogApproveDialogComponent, {
      width: '400px',
      data: {state}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (state === 1) {
          this.accountApplicationService.updateRequestState(this.idDriver, true, this.commentValue, result => {
            if (result) {
              this.openSnackBarAccept('Водитель одобрен');
              this.router.navigate(['/main/drivers/moderation']);
            } else {
              this.openSnackBarAccept('Произошла ошибка');
            }
          });

        } else {
          this.accountApplicationService.updateRequestState(this.idDriver, false, this.commentValue, result => {
            if (result) {
              this.openSnackBarAccept('Водитель отклонен');
              this.router.navigate(['/main/drivers/moderation']);
            } else {
              this.openSnackBarAccept('Произошла ошибка');
            }
          });
        }
      }
    });
  }

  cancel() {
    this.router.navigate(['/main/drivers/moderation']);
  }

  reject() {
    this.openDialog(0);
  }

  accept() {
    this.openDialog(1);
  }
}

interface DialogData {
  state: number;
}

@Component({
  templateUrl: 'dialog-approve-dialog.html',
})
export class DialogApproveDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogApproveDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onNoClick(b: boolean) {
    this.dialogRef.close(b);
  }

  onClearClick(b: boolean) {
    this.dialogRef.close(b);
  }
}


