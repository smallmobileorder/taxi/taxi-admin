import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MainComponent} from '../main/main.component';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';
import {AccountApplicationShort, AccountApplicationShortTable} from '../../models/accountApplication';
import {AccountApplicationService} from '../../services/account-application.service';
import {DateAdapter, MAT_DATE_LOCALE} from '@angular/material/core';


const ELEMENT_DATA: AccountApplicationShort[] = [];

@Component({
  selector: 'app-moderation',
  templateUrl: './moderation.component.html',
  styleUrls: ['./moderation.component.css'],
  providers: [{provide: MAT_DATE_LOCALE, useValue: 'ru'}]
})
export class ModerationComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['date_update', 'date_writing', 'status', 'id', 'fio', 'phone'];
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  private order = 'person.id';
  private direction = 'desc';
  private readonly data: any;
  private accountApplicationShortTable: AccountApplicationShortTable;

  constructor(private mainComponent: MainComponent, private accountApplicationService: AccountApplicationService,
              private route: ActivatedRoute, private router: Router, private adapter: DateAdapter<any>) {
    adapter.setLocale('ru');
    this.data = this.route.snapshot.data.data;
  }

  dataSource = new MatTableDataSource(ELEMENT_DATA);
  length = 0;
  pageSize = 15;
  pageSizeOptions: number[] = [5, 10, 15];

  requestString = '';
  mapSortKeys: Map<string, string>;

  ngOnInit() {
    this.accountApplicationShortTable = this.accountApplicationService.getAccountApplicationDataShort(this.data);
    this.refreshTable(this.accountApplicationShortTable);
    console.log(this.data);
    this.mainComponent.initActiveNode('Модерация заявок', 'main/drivers/moderation');
  }


  ngAfterViewInit(): void {
    this.mapSortKeys = new Map();
    this.dataSource.sort = this.sort;
    this.sort.disableClear = true;
    this.mapSortKeys.set('id', 'person.id').set('fio', 'first_name').set('isBlock', '');
    this.initSortListener();
    this.initPageListener();
  }

  initSortListener() {
    this.sort.sortChange.subscribe(() => {
      if (this.sort.direction !== '') {
        this.order = this.mapSortKeys.get(this.sort.active);
        this.direction = this.sort.direction;
        this.paginator.pageIndex = 0;
        this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
          '0');
      }
    });
  }

  initPageListener() {
    this.paginator.page.subscribe(() => {
      this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
        (this.paginator.pageSize * this.paginator.pageIndex).toString());
    });
  }

  filterEvent(filterValue: string) {
    if (this.requestString !== filterValue.trim()) {
      this.requestString = filterValue.trim();
      this.request(filterValue.trim(), 'person.id', 'DESC', this.paginator.pageSize.toString(),
        '0');
    }
  }

  request(filter: string, order: string, direction: string, limit: string, offset: string) {
    this.accountApplicationService.getModerationList(filter, order, direction, limit, offset, result => {
      this.accountApplicationShortTable = result as AccountApplicationShortTable;
      this.refreshTable(this.accountApplicationShortTable);
    });
  }

  refreshTable(accountApplicationShortTable: AccountApplicationShortTable) {
    this.dataSource = new MatTableDataSource(accountApplicationShortTable.accounts);
    this.dataSource.sort = this.sort;
    this.length = accountApplicationShortTable.length;
  }

  goToRegisterDriver(element: AccountApplicationShort) {
    this.router.navigate(['main/drivers/approved/' + element.id]);
  }
}


