import {Component, NgZone, OnInit} from '@angular/core';
import {MainComponent} from '../main/main.component';
import {ActivatedRoute} from '@angular/router';
import {Config, DriversTariffsConfigs} from '../../models/config';
import {ConfigService} from '../../services/config.service';
import {FormControl, NgForm, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-main-config',
  templateUrl: './main-config.component.html',
  styleUrls: ['./main-config.component.css']
})
export class MainConfigComponent implements OnInit {
  private readonly data: any;
  private mainConfigs: Config;

  constructor(private mainComponent: MainComponent, private mainConfigsService: ConfigService,
              private activateRoute: ActivatedRoute, private snackBar: MatSnackBar) {
    this.data = this.activateRoute.snapshot.data.data;
  }

  fontSize = '12px';
  commissionForFineForm = new FormControl('', [Validators.required]);
  amountFineForm = new FormControl('', [Validators.required]);
  minBalanceDriverForm = new FormControl('', [Validators.required]);
  maxCountHaltForm = new FormControl('', [Validators.required]);
  timeFreeWaitingForm = new FormControl('', [Validators.required]);
  priceForHaltForm = new FormControl('', [Validators.required]);
  priceForHaltInWayForm = new FormControl('', [Validators.required]);
  timeForCancelOrderWithoutDriverForm = new FormControl('', [Validators.required]);
  minBalanceDriverForAcceptOrderForm = new FormControl('', [Validators.required]);
  timeForFinishOrderForm = new FormControl('', [Validators.required]);
  timeForFinishOrderWithoutRatingForm = new FormControl('', [Validators.required]);
  timeForCheckStatusCallClientForm = new FormControl('', [Validators.required]);
  intervalForCheckStatusCallClientForm = new FormControl('', [Validators.required]);
  countForCheckStatusCallClientForm = new FormControl('', [Validators.required]);
  timeForCancelOrderForm = new FormControl('', [Validators.required]);
  callForUpPriceForm = new FormControl('', [Validators.required]);
  priceForMinWaitingForm = new FormControl('', [Validators.required]);
  commissionForFine = '';
  amountFine = '';
  minBalanceDriver = '';
  maxCountHalt = '';
  timeFreeWaiting = '';
  priceForHalt = '';
  priceForHaltInWay = '';
  timeForCancelOrderWithoutDriver = '';
  minBalanceDriverForAcceptOrder = '';
  timeForFinishOrder = '';
  timeForFinishOrderWithoutRating = '';
  timeForCheckStatusCallClient = '';
  intervalForCheckStatusCallClient = '';
  countForCheckStatusCallClient = '';
  timeForCancelOrder = '';
  callForUpPrice = '';
  priceForMinWaiting = '';

  ngOnInit() {
    this.mainComponent.initActiveNode('Главная конфигурация', 'main/config/main-config');
    this.mainConfigs = this.mainConfigsService.getMainConfigData(this.data);
    this.fillComponent();
  }

  private fillComponent() {
    this.commissionForFine = this.mainConfigs.commission_for_fine;
    this.amountFine
      = this.mainConfigs.amount_fine;
    this.minBalanceDriver
      = this.mainConfigs.min_balance_driver;
    this.maxCountHalt
      = this.mainConfigs.max_count_halt;
    this.timeFreeWaiting
      = this.mainConfigs.time_free_waiting;
    this.priceForHalt
      = this.mainConfigs.price_for_halt;
    this.priceForHaltInWay
      = this.mainConfigs.price_for_halt_in_way;
    this.timeForCancelOrderWithoutDriver
      = this.mainConfigs.time_for_cancel_order_without_driver;
    this.minBalanceDriverForAcceptOrder
      = this.mainConfigs.min_balance_driver_for_accept_order;
    this.timeForFinishOrder
      = this.mainConfigs.time_for_finish_order;
    this.timeForFinishOrderWithoutRating
      = this.mainConfigs.time_for_finish_order_without_rating;
    this.timeForCheckStatusCallClient
      = this.mainConfigs.time_for_check_status_call_client;
    this.intervalForCheckStatusCallClient
      = this.mainConfigs.interval_for_check_status_call_client;
    this.countForCheckStatusCallClient
      = this.mainConfigs.count_for_check_status_call_client;
    this.timeForCancelOrder
      = this.mainConfigs.time_for_cancel_order;
    this.callForUpPrice
      = this.mainConfigs.call_for_up_price;
    this.priceForMinWaiting
      = this.mainConfigs.price_for_min_waiting;
  }

  transformSecondToTime(secondTime: string): string {
    const seconds = Number.parseInt(secondTime, 10);
    const date = new Date(seconds);
    date.setSeconds(seconds);
    return date.toISOString().substr(11, 8);
  }

  onSubmit(form: NgForm) {
    console.log(form);
    const mainConfigForm = {
      commission_for_fine: this.commissionForFineForm.value,
      amount_fine: this.amountFineForm.value,
      min_balance_driver: this.minBalanceDriverForm.value,
      max_count_halt: this.maxCountHaltForm.value,
      time_free_waiting: this.timeFreeWaitingForm.value,
      price_for_halt: this.priceForHaltForm.value,
      price_for_halt_in_way: this.priceForHaltInWayForm.value,
      time_for_cancel_order_without_driver: this.transformSecondToTime(this.timeForCancelOrderWithoutDriverForm.value),
      min_balance_driver_for_accept_order: this.minBalanceDriverForAcceptOrderForm.value,
      time_for_finish_order: this.transformSecondToTime(this.timeForFinishOrderForm.value),
      time_for_finish_order_without_rating: this.transformSecondToTime(this.timeForFinishOrderWithoutRatingForm.value),
      time_for_check_status_call_client: this.transformSecondToTime(this.timeForCheckStatusCallClientForm.value),
      interval_for_check_status_call_client: this.transformSecondToTime(this.intervalForCheckStatusCallClientForm.value),
      count_for_check_status_call_client: this.countForCheckStatusCallClientForm.value,
      time_for_cancel_order: this.transformSecondToTime(this.timeForCancelOrderForm.value),
      call_for_up_price: this.transformSecondToTime(this.callForUpPriceForm.value),
      price_for_min_waiting: this.priceForMinWaitingForm.value,
      id_city: 1,
      life_time_rates: '',
      cancel_order_without_call: '',
    };
    const resultUpdate = mainConfigForm as Config;
    this.mainConfigsService.updateConfigs(resultUpdate, result => {
      this.openSnackBar(result);
    });
  }

  openSnackBar(success: boolean) {
    if (success) {
      this.snackBar.open('Данные обновлены', '', {
        duration: 2000,
        panelClass: ['snackbar-container']
      });
    } else {
      this.snackBar.open('Произошла ошибка при обновлении данных', '', {
        duration: 2000,
        panelClass: ['snackbar-container']
      });
    }
  }
}
