import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MainComponent} from '../main/main.component';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ClientsService} from '../../services/clients.service';
import {Client, ClientTable} from '../../models/client';
import {ActivatedRoute} from '@angular/router';


const ELEMENT_DATA: Client[] = [];

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['id', 'name', 'phone', 'isBlock'];
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  private order = 'person.id';
  private direction = 'desc';
  private readonly data: any;
  private clientTable: ClientTable;

  constructor(private mainComponent: MainComponent, private clientService: ClientsService, private route: ActivatedRoute) {
    this.data = this.route.snapshot.data.data;
  }

  dataSource = new MatTableDataSource(ELEMENT_DATA);
  length = 0;
  pageSize = 15;
  pageSizeOptions: number[] = [5, 10, 15];

  requestString = '';
  mapSortKeys: Map<string, string>;

  ngOnInit() {
    this.clientTable = this.clientService.getClientTable(this.data);
    this.refreshTable(this.clientTable);
    this.mainComponent.initActiveNode('Клиенты', 'main/database/clients');
  }


  ngAfterViewInit(): void {
    this.mapSortKeys = new Map();
    this.dataSource.sort = this.sort;
    this.sort.disableClear = true;
    this.mapSortKeys.set('id', 'person.id').set('name', 'first_name').set('isBlock', 'black_list.id');
    this.initSortListener();
    this.initPageListener();
  }

  initSortListener() {
    this.sort.sortChange.subscribe(() => {
      if (this.sort.direction !== '') {
        this.order = this.mapSortKeys.get(this.sort.active);
        this.direction = this.sort.direction;
        this.paginator.pageIndex = 0;
        this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
          '0');
      }
    });
  }

  initPageListener() {
    this.paginator.page.subscribe(() => {
      this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
        (this.paginator.pageSize * this.paginator.pageIndex).toString());
    });
  }

  filterEvent(filterValue: string) {
    if (this.requestString !== filterValue.trim()) {
      this.requestString = filterValue.trim();
      this.request(filterValue.trim(), 'person.id', 'desc', this.paginator.pageSize.toString(),
        '0');
    }
  }

  request(filter: string, order: string, direction: string, limit: string, offset: string) {
    this.clientService.getClients(filter, order, direction, limit, offset, result => {
      this.clientTable = result as ClientTable;
      this.refreshTable(this.clientTable);
    });
  }

  refreshTable(clientTable: ClientTable) {
    this.dataSource = new MatTableDataSource(clientTable.clients);
    this.dataSource.sort = this.sort;
    this.length = clientTable.length;
  }
}
