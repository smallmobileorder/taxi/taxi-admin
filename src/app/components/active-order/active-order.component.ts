import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MainComponent} from '../main/main.component';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import {OrderActive, OrderActiveTable, StateOrder} from '../../models/OrderActive';
import {OrderService} from '../../services/order.service';
import {catchError, delay} from "rxjs/operators";
import {EMPTY, forkJoin} from "rxjs";


@Component({
  selector: 'app-active-order',
  templateUrl: './active-order.component.html',
  styleUrls: ['./active-order.component.css']
})
export class ActiveOrderComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  private order = 'orders.date_create';
  private direction = 'desc';
  private data: any;
  private orderTable: OrderActiveTable;

  constructor(private mainComponent: MainComponent, private orderService: OrderService, private route: ActivatedRoute,
              private router: Router) {
    this.data = this.route.snapshot.data.data;

  }

  displayedColumns: string[] = ['number_order', 'timer', 'addressDepart', 'addressDest', 'driver', 'phone', 'client'];
  allOrders: MatTableDataSource<OrderActive>;
  findingOrders: MatTableDataSource<OrderActive>;
  waitingOrders: MatTableDataSource<OrderActive>;
  tripOrders: MatTableDataSource<OrderActive>;
  // MatPaginator Inputs
  length = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10];

  requestString = '';
  mapSortKeys: Map<string, string>;
  private isNeed = true;

  ngOnInit() {
    this.orderTable = this.orderService.getOrdersActiveTable(this.data);
    console.log(this.orderTable);
    this.refreshTable(this.orderTable);
    this.mainComponent.initActiveNode('Активные заказы', '/call-center/working/active-orders');
  }

  ngAfterViewInit(): void {
    this.mapSortKeys = new Map();
    this.allOrders.sort = this.sort;
    this.sort.disableClear = true;
    this.mapSortKeys.set('number_order', 'orders.id').set('date_finish', 'orders.data_start_finding');
    this.sort.sortChange.subscribe(() => {
      if (this.sort.direction !== '') {
        this.order = this.mapSortKeys.get(this.sort.active);
        this.direction = this.sort.direction;
        this.paginator.pageIndex = 0;
        this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
          '0');
      }
    });
    this.paginator.page.subscribe(() => {
      this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
        (this.paginator.pageSize * this.paginator.pageIndex).toString());
    });

    // while (this.isNeed) {
    //   delay(5);
    //
    //   forkJoin(this.orderService.getOrdersObservable(this.requestString, this.order, this.sort.direction,
    //     this.paginator.pageSize.toString(),
    //     (this.paginator.pageSize * this.paginator.pageIndex).toString(), StateOrder.FINDING).pipe(catchError(() => {
    //       return EMPTY;
    //     })),
    //     this.orderService.getOrdersObservable(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
    //       (this.paginator.pageSize * this.paginator.pageIndex).toString(), StateOrder.WAITING).pipe(catchError(() => {
    //       return EMPTY;
    //     })),
    //     this.orderService.getOrdersObservable(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
    //       (this.paginator.pageSize * this.paginator.pageIndex).toString(), StateOrder.TRIP).pipe(catchError(() => {
    //       return EMPTY;
    //     }))
    //   ).subscribe(result => {
    //     this.orderTable = this.orderService.getOrdersActiveTable(result);
    //     this.refreshTable(this.orderTable);
    //   });
    // }
  }

  request(filter: string, order: string, direction: string, limit: string, offset: string) {
    this.orderService.getOrdersActive(filter, order, direction, limit, offset, StateOrder.WAITING, result => {
      this.orderTable = result as OrderActiveTable;
      this.refreshTable(this.orderTable);
    });
  }

  refreshTable(orderTable: OrderActiveTable) {
    this.findingOrders = new MatTableDataSource(this.orderTable.ordersFinding);
    this.waitingOrders = new MatTableDataSource(this.orderTable.ordersWaiting);
    this.tripOrders = new MatTableDataSource(this.orderTable.ordersTrip);
    this.allOrders = new MatTableDataSource(this.orderTable.ordersTrip.concat(this.orderTable.ordersWaiting,
      this.orderTable.ordersFinding));
    this.allOrders.sort = this.sort;
    this.waitingOrders.sort = this.sort;
    this.tripOrders.sort = this.sort;
    this.findingOrders.sort = this.sort;
  }


  applyFilter(filterValue: string) {
    if (this.requestString !== filterValue.trim()) {
      this.requestString = filterValue.trim();
      this.request(filterValue.trim(), 'orders.date_create', 'desc', this.paginator.pageSize.toString(),
        '0');
    }
  }

  selectRow(numberOrder: string) {
    this.router.navigate(['main/call-center/working/order-info/' + numberOrder]);
  }
}
