import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {Driver, DriverTable} from '../../models/driver';
import {MainComponent} from '../main/main.component';
import {DriversService} from '../../services/drivers.service';
import {MatTableDataSource} from '@angular/material/table';
import {AddTransactionService} from '../../services/add-transaction.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatHorizontalStepper} from '@angular/material/stepper';
import {TYPE_BALANCE, TYPE_OBJECT, TYPE_OPERATION} from '../../models/transaction';
import {delay} from "rxjs/operators";
import {Observable} from "rxjs";


const ELEMENT_DATA: Driver[] = [];


@Component({
  selector: 'app-add-transaction',
  templateUrl: './add-transaction.component.html',
  styleUrls: ['./add-transaction.component.scss']
})
export class AddTransactionComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  private order = 'person.id';
  private direction = 'desc';
  private data: any;
  private driverTable: DriverTable;
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;


  constructor(private mainComponent: MainComponent, private transactionService: AddTransactionService,
              private driverService: DriversService, private route: ActivatedRoute,
              private router: Router, private formBuilder: FormBuilder, private snackBar: MatSnackBar) {
    this.data = this.route.snapshot.data.data;
  }

  displayedColumns: string[] = ['id', 'name', 'phone', 'balance', 'auto', 'isBlock'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  // MatPaginator Inputs
  length = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10];

  requestString = '';
  mapSortKeys: Map<string, string>;
  summAdd = '';
  summAddForm = new FormControl('', [Validators.required]);
  commentValue = '';
  selectedId = '0';
  isErr = false;

  ngOnInit() {
    this.driverTable = this.driverService.getDriverTable(this.data);
    this.refreshTable(this.driverTable);
    this.mainComponent.initActiveNode('Пополнение баланса', 'main/database/add-transaction');
    this.firstFormGroup = this.formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  ngAfterViewInit(): void {
    this.mapSortKeys = new Map();
    this.dataSource.sort = this.sort;
    this.sort.disableClear = true;
    this.mapSortKeys.set('id', 'person.id').set('name', 'first_name').set('phone', 'phone_number')
      .set('balance', 'balance').set('auto', 'car_number').set('isBlock', 'black_list.id');
    this.sort.sortChange.subscribe(() => {
      if (this.sort.direction !== '') {
        this.order = this.mapSortKeys.get(this.sort.active);
        this.direction = this.sort.direction;
        this.paginator.pageIndex = 0;
        this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
          '0');
      }
    });
    this.paginator.page.subscribe(() => {
      this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
        (this.paginator.pageSize * this.paginator.pageIndex).toString());
    });
  }

  request(filter: string, order: string, direction: string, limit: string, offset: string) {
    this.driverService.getDrivers(filter, order, direction, limit, offset, result => {
      this.driverTable = result as DriverTable;
      this.refreshTable(this.driverTable);
    });
  }

  refreshTable(driverTable: DriverTable) {
    this.dataSource = new MatTableDataSource(driverTable.drivers);
    this.selectedId = '0';
    this.dataSource.sort = this.sort;
    this.length = driverTable.length;
  }


  applyFilter(filterValue: string) {
    if (this.requestString !== filterValue.trim()) {
      this.requestString = filterValue.trim();
      this.request(filterValue.trim(), 'person.id', 'desc', this.paginator.pageSize.toString(),
        '0');
    }
  }

  accept() {
    if (!this.summAddForm.invalid) {
      this.transactionService.addTransaction(new Date().toLocaleDateString().split('.').join('-'), this.selectedId, this.summAdd,
        TYPE_OPERATION.ADD_BALANCE, 'В офисе', TYPE_OBJECT.WALLET, TYPE_BALANCE.BALANCE_DRIVER, this.commentValue, result => {
          this.openSnackBar(result);
        }
      );
    }
  }

  openSnackBar(success: boolean) {
    if (success) {
      this.snackBar.open('Баланс пополнен', '', {
        duration: 1000,
        panelClass: ['snackbar-container']
      });
      new Observable().pipe(delay(1000)).subscribe(value => {
        this.router.navigate(['main/database/transactions']);
      });
    } else {
      this.snackBar.open('Произошла ошибка при внесении баланса', '', {
        duration: 2000,
        panelClass: ['snackbar-container']
      });
    }
  }

  cancel() {
    this.router.navigate(['main/database/transactions']);
  }

  onSubmit(stepper: MatHorizontalStepper) {
    if (this.selectedId !== '0') {
      stepper.next();
    } else {
      this.isErr = true;
    }
  }

  selectRow(rowId: string) {
    this.isErr = false;
    this.selectedId = rowId;
  }
}
