import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MainComponent} from '../main/main.component';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {TransactionsService} from '../../services/transactions.service';
import {Transaction, TransactionTable} from '../../models/transaction';
import {ActivatedRoute, Router} from '@angular/router';

const ELEMENT_DATA: Transaction[] = [];

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['date', 'id', 'phone', 'balance_before', 'sum', 'balance_after', 'type_operation', 'payment_system',
    'type_object', 'type_balance', 'comment'];
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  private order = 'person.id';
  private direction = 'desc';
  private readonly data: any;
  private transactionTable: TransactionTable;

  constructor(private mainComponent: MainComponent, private transactionService: TransactionsService, private route: ActivatedRoute,
              private router: Router) {
    this.data = this.route.snapshot.data.data;
  }

  dataSource = new MatTableDataSource(ELEMENT_DATA);
  length = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10];
  requestString = '';
  mapSortKeys: Map<string, string>;


  ngOnInit() {
    this.transactionTable = this.transactionService.getTransactionTable(this.data);
    this.refreshTable(this.transactionTable);
    this.mainComponent.initActiveNode('Транзакции', 'main/database/transactions');
  }

  ngAfterViewInit(): void {
    this.mapSortKeys = new Map();
    this.dataSource.sort = this.sort;
    this.sort.disableClear = true;
    this.mapSortKeys.set('id', 'person.id').set('date', 'timestamp').set('payment_system', 'payment_system')
      .set('type_object', 'type_object').set('type_balance', 'type_balance').set('balance_before', 'balance_before').set('sum', 'amount')
      .set('type_operation', 'type_operation');
    this.initSortListener();
    this.initPageListener();
  }

  private initSortListener() {
    this.sort.sortChange.subscribe(() => {
      if (this.sort.direction !== '') {
        this.order = this.mapSortKeys.get(this.sort.active);
        this.direction = this.sort.direction;
        this.paginator.pageIndex = 0;
        this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
          '0');
      }
    });
  }

  private initPageListener() {
    this.paginator.page.subscribe(() => {
      this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
        (this.paginator.pageSize * this.paginator.pageIndex).toString());
    });
  }

  filterEvent(filterValue: string) {
    if (this.requestString !== filterValue.trim()) {
      this.requestString = filterValue.trim();
      this.request(filterValue.trim(), 'timestamp', 'desc', this.paginator.pageSize.toString(),
        '0');
    }
  }

  request(filter: string, order: string, direction: string, limit: string, offset: string) {
    this.transactionService.getTransaction(filter, order, direction, limit, offset, result => {
      this.transactionTable = result as TransactionTable;
      this.refreshTable(this.transactionTable);
    });
  }

  private refreshTable(transactionTable: TransactionTable) {
    this.dataSource = new MatTableDataSource(transactionTable.transactions);
    this.dataSource.sort = this.sort;
    this.length = transactionTable.length;
  }

  addBalance() {
    this.router.navigate(['main/database/add-transaction']);
  }
}
