import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router, RouterEvent} from '@angular/router';
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatSidenav, MatTreeNestedDataSource} from '@angular/material';

interface MenuNode {
  url: string;
  name: string;
  children?: MenuNode[];
}

const TREE_DATA: MenuNode[] = [
  {
    url: 'main/database',
    name: 'Справочники',
    children: [
      {
        url: 'main/database/drivers',
        name: 'Водители'
      },
      {
        url: 'main/database/clients',
        name: 'Клиенты'
      },
      {
        url: 'main/database/transactions',
        name: 'Касса'
      },
    ]
  }, {
    url: 'main/drivers',
    name: 'Водители',
    children: [
      {
        url: 'main/drivers/registration-internal',
        name: 'Очное трудоустройство'
      },
      {
        url: 'main/drivers/moderation', name: 'Прием заявок'
      }
    ]
  },
  {
    url: 'main/call-center',
    name: 'Call-центр',
    children: [
      {
        url: 'main/call-center/working',
        name: 'Рабочее место',
        children: [
          {url: 'main/call-center/working/complete-order', name: 'Завершенные заказы'},
          {url: 'main/call-center/working/active-order', name: 'Активные заказы'},
          {url: 'main/call-center/working/support', name: 'Техническая поддержка'},
          {url: 'main/call-center/working/live-driver', name: 'Водители на карте'}
        ]
      },
    ]
  },
  {
    url: 'main/config',
    name: 'Конфигурация',
    children: [
      {
        url: 'main/config/main-config', name: 'Главная конфигурация'
      },
      {
        url: 'main/config/tariffs-config', name: 'Конфигурация тарифов',
      }
    ]
  }
];


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  public loading = true;

  constructor(private router: Router) {
    this.dataSource.data = TREE_DATA;
    router.events.subscribe((routerEvent: RouterEvent) => {
      this.checkRouterEvent(routerEvent);
    });
  }

  @Output() sidenavClose = new EventEmitter();
  public activeNode: MenuNode;

  treeControl = new NestedTreeControl<MenuNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<MenuNode>();
  color = 'primary';
  mode = 'indeterminate';
  value = 50;
  bufferValue = 75;


  hasChild = (_: number, node: MenuNode) => !!node.children && node.children.length > 0;

  rout(node: any, sidenav: MatSidenav) {
    this.activeNode = node;
    this.onSidenavEvent(sidenav);
    this.router.navigateByUrl(node.url);
  }


  ngOnInit(): void {
    this.activeNode = {
      children: null,
      name: 'Админка',
      url: this.router.url,
    };
  }

  initActiveNode(name: string, url: string) {
    this.activeNode = {
      children: null,
      name,
      url,
    };
  }


  onSidenavEvent(sidenav: MatSidenav) {
    if (sidenav.opened) {
      sidenav.close();
    } else {
      sidenav.open();
    }
  }

  private checkRouterEvent(routerEvent: RouterEvent) {
    if (routerEvent instanceof NavigationStart) {
      this.loading = true;
    }

    if (routerEvent instanceof NavigationEnd ||
      routerEvent instanceof NavigationCancel ||
      routerEvent instanceof NavigationError) {
      this.loading = false;
    }
  }
}
