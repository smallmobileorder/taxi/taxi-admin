import {Component, OnInit} from '@angular/core';
import {FormControl, NgForm, Validators} from '@angular/forms';
import {DriversTariffsConfigs} from '../../models/config';
import {MainComponent} from '../main/main.component';
import {ConfigService} from '../../services/config.service';
import {ActivatedRoute} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TariffsConfigService} from '../../services/tariffs-config.service';



@Component({
  selector: 'app-tariffs-config',
  templateUrl: './tariffs-config.component.html',
  styleUrls: ['./tariffs-config.component.scss']
})
export class TariffsConfigComponent implements OnInit {
  fontSize = '12px';
  private driversConfigs: DriversTariffsConfigs;
  private data: any;

  minPriceForm = new FormControl('', [Validators.required]);
  priceForDistanceForm = new FormControl('', [Validators.required]);
  priceForTimeForm = new FormControl('', [Validators.required]);

  constructor(private mainComponent: MainComponent, private tariffsConfigService: TariffsConfigService,
              private activateRoute: ActivatedRoute, private snackBar: MatSnackBar) {
    this.data = this.activateRoute.snapshot.data.data;
  }

  ngOnInit() {
    this.mainComponent.initActiveNode('Конфигурация тарифов', 'main/config/tariffs-config');
    this.driversConfigs = this.tariffsConfigService.getDriversTariffsConfigs(this.data);
    this.fillComponents();
  }

  fillComponents() {

  }

  onSubmit(formMainConfig: NgForm) {

  }

  openSnackBar(success: boolean) {
    if (success) {
      this.snackBar.open('Данные обновлены', '', {
        duration: 2000,
        panelClass: ['snackbar-container']
      });
    } else {
      this.snackBar.open('Произошла ошибка при обновлении данных', '', {
        duration: 2000,
        panelClass: ['snackbar-container']
      });
    }
  }
}
