import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {DatePipe} from '@angular/common';
import {
  ErrorStateMatcher,
  MAT_DIALOG_DATA,
  MAT_RADIO_DEFAULT_OPTIONS,
  MatDialog,
  MatDialogRef,
  MatRadioButton,
  MatSnackBar
} from '@angular/material';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {MainComponent} from '../main/main.component';
import {RegisterUserService} from '../../services/register-user.service';
import {DateAdapter, MAT_DATE_LOCALE} from '@angular/material/core';

@Component({
  selector: 'app-registration-driver',
  templateUrl: './registration-driver.component.html',
  styleUrls: ['./registration-driver.component.scss'],
  providers: [DatePipe, {
    provide: MAT_RADIO_DEFAULT_OPTIONS,
    useValue: {color: 'primary'}
  },
    {provide: MAT_DATE_LOCALE, useValue: 'ru'}]

})


export class RegistrationDriverComponent implements OnInit {
  durationInSeconds = 5;
  @ViewChild('rad1', {static: false}) rad1: MatRadioButton;
  @ViewChild('rad2', {static: false}) rad2: MatRadioButton;

  constructor(private mainComponent: MainComponent, public dialog: MatDialog, private snackBar: MatSnackBar,
              private registerDriver: RegisterUserService, private adapter: DateAdapter<any>) {
    adapter.setLocale('ru');
  }

  minDate = new Date();
  maxDate = new Date();

  firstNameFormControl = new FormControl('', [Validators.required, Validators.pattern('^([a-zA-Zа-яА-я]*)$')]);
  secondNameFormControl = new FormControl('', [Validators.required, Validators.pattern('^([a-zA-Zа-яА-я]*)$')]);
  patronymicFormControl = new FormControl('', [Validators.required, Validators.pattern('^([a-zA-Zа-яА-я]*)$')]);
  emailFormControl = new FormControl('', [Validators.email]);
  addressFormControl = new FormControl('', [Validators.required]);
  phoneFormControl = new FormControl('', [Validators.required, Validators.maxLength(10), Validators.pattern('^([0-9]{10})')]);
  dateFormControl = new FormControl('', [Validators.required]);
  seriaPasportFormControl = new FormControl('', [Validators.required, Validators.maxLength(4), Validators.minLength(4), Validators.pattern('^([0-9]){4}')]);
  seriaVodFormControl = new FormControl('', [Validators.required, Validators.maxLength(4), Validators.minLength(4), Validators.pattern('^([0-9]){4}')]);
  numberVodFormControl = new FormControl('', [Validators.required, Validators.minLength(4), Validators.pattern('^([0-9])*')]);
  numberPasportFormControl = new FormControl('', [Validators.required, Validators.minLength(4), Validators.pattern('^([0-9])*')]);
  dateVidachiFormControl = new FormControl('', [Validators.required]);
  dateVidachiVodFormControl = new FormControl('', [Validators.required]);
  dateAccessFormControl = new FormControl('', [Validators.required]);
  dateCurrentFormControl = new FormControl(new Date(), [Validators.required]);
  timeCurrentFormControl = new FormControl('', [Validators.required]);
  markFormControl = new FormControl('', [Validators.required]);
  modelFormControl = new FormControl('', [Validators.required]);
  colorFormControl = new FormControl('', [Validators.required, Validators.pattern('^([a-zA-Zа-яА-я]*)$')]);
  yearMakingFormControl = new FormControl('', [Validators.required, Validators.pattern('([0-9]){4}')]);
  numberAutoFormControl = new FormControl('', [Validators.required]);
  countDoorsFormControl = new FormControl('', [Validators.required, Validators.pattern('([0-9]){1}')]);
  vinFormControl = new FormControl('', [Validators.required, Validators.pattern('([0-9])*')]);
  addressFormControl2 = new FormControl('');
  success: boolean;
  name: string;
  matcher = new MyErrorStateMatcher(false);
  currentDate = new Date();
  currentTimeWriting = '00:00';
  expand1 = false;

  expand2 = false;
  expand3 = false;
  firstNameValue = '';
  secondNameValue = '';
  patronymicNameValue = '';
  birthdayValue = '';
  addressValue = '';
  addressValue2 = '';
  phoneValue = '';
  emailValue = '';
  seriaPasportValue = '';
  numberPasportValue = '';
  datePasportValue = '';
  seriaVodValue = '';
  numberVodValue = '';
  dateVodValue = '';
  dateAccessValue = '';
  markValue = '';
  modelValue = '';
  colorValue = '';
  numberValue = '';
  yearValue = '';
  countDoorValue = '';
  vinValue = '';
  commentValue = '';

  ngOnInit() {
    this.mainComponent.initActiveNode('Очное трудоустройство', 'main/drivers/registration-internal');
    this.currentTimeWriting = this.currentDate.getHours() + ':' + this.currentDate.getMinutes();
    this.minDate = new Date(this.minDate.getFullYear() - 100, this.minDate.getMonth(), this.minDate.getDay());
    this.maxDate = new Date(this.maxDate.getFullYear() - 18, this.maxDate.getMonth(), this.maxDate.getDay());
    this.expand1 = !this.expand1;
  }

  addUser() {
    if (this.checkValid()) {
      this.registerDriver.addDriver(this.firstNameFormControl.value, this.secondNameFormControl.value,
        this.patronymicFormControl.value, new Date(this.dateFormControl.value).toLocaleDateString(),
        this.addressFormControl.value, this.addressFormControl.value, this.phoneFormControl.value,
        this.emailFormControl.value, this.seriaPasportFormControl.value, this.numberPasportFormControl.value,
        new Date(this.dateVidachiFormControl.value).toLocaleDateString(), this.seriaVodFormControl.value,
        this.numberVodFormControl.value, new Date(this.dateVidachiVodFormControl.value).toLocaleDateString(),
        new Date(this.dateAccessFormControl.value).toLocaleDateString(), this.markFormControl.value,
        this.modelFormControl.value, this.colorFormControl.value, this.numberAutoFormControl.value,
        this.yearMakingFormControl.value, this.countDoorsFormControl.value, this.vinFormControl.value,
        this.commentValue, this.rad1.checked, this.currentTimeWriting,
        new Date(this.dateCurrentFormControl.value).toLocaleDateString(), result => {
          this.openSnackBar(result);
        });
    }
  }

  private checkValid() {
    this.matcher.setAdd(true);
    const form1 = this.checkValid1();
    if (form1) {
      const form2 = this.checkValid2();
      if (form2) {
        const form3 = this.checkValid3();
        if (form3) {
          return (new Date(this.dateVidachiVodFormControl.value).toLocaleDateString().trim().length > 0)
            && (this.currentTimeWriting.trim().length > 0);
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  private checkValid1() {
    const form1 = this.emailFormControl.valid && this.firstNameFormControl.valid && this.secondNameFormControl
      && this.addressFormControl.valid && this.phoneFormControl.valid && this.dateFormControl.valid;
    if (!form1) {
      this.expand1 = true;
    }
    return form1;
  }

  private checkValid2() {
    const form2 = this.seriaPasportFormControl.valid && this.seriaVodFormControl.valid && this.numberVodFormControl.valid
      && this.numberPasportFormControl.valid && this.dateVidachiFormControl.valid && this.dateVidachiVodFormControl.valid
      && this.dateAccessFormControl.valid;
    if (!form2) {
      this.expand2 = false;
    }
    return form2;
  }

  private checkValid3() {
    const form3 = this.markFormControl.valid && this.modelFormControl.valid && this.colorFormControl.valid
      && this.yearMakingFormControl.valid && this.numberAutoFormControl.valid && this.countDoorsFormControl.valid
      && this.vinFormControl.valid;
    if (!form3) {
      this.expand3 = false;
    }
    return form3;
  }

  openSnackBar(success: boolean) {
    if (success) {
      this.snackBar.open('Водитель добавлен на проверку', '', {
        duration: this.durationInSeconds * 1000,
        panelClass: ['snackbar-container']
      });
    } else {
      this.snackBar.open('Произошла ошибка, попробуйте перезагрузить страницу', '', {
        duration: this.durationInSeconds * 1000,
        panelClass: ['snackbar-container']
      });
    }
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialogComponent, {
      width: '250px',
      data: {success: this.success}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result) {
        this.clearInputs();
      }
    });
  }

  clearInputs() {
    this.firstNameValue = ' ';
    this.secondNameValue = ' ';
    this.patronymicNameValue = ' ';
    this.birthdayValue = ' ';
    this.addressValue = ' ';
    this.addressValue2 = ' ';
    this.phoneValue = ' ';
    this.emailValue = ' ';
    this.seriaPasportValue = ' ';
    this.numberPasportValue = ' ';
    this.datePasportValue = ' ';
    this.seriaVodValue = ' ';
    this.numberVodValue = ' ';
    this.dateVodValue = ' ';
    this.dateAccessValue = ' ';
    this.markValue = ' ';
    this.modelValue = ' ';
    this.colorValue = ' ';
    this.numberValue = ' ';
    this.yearValue = ' ';
    this.countDoorValue = ' ';
    this.vinValue = ' ';
    this.commentValue = ' ';
  }
}

interface DialogData {
  success: boolean;
}

@Component({
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onNoClick(b: boolean) {
    this.dialogRef.close(b);
  }

  onClearClick(b: boolean) {
    this.dialogRef.close(b);
  }
}


/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  constructor(private isAdd: boolean) {
  }

  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted || this.isAdd));
  }

  public setAdd(isAdd: boolean) {
    this.isAdd = isAdd;
  }
}


