import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MainComponent} from '../main/main.component';
import {OrderFinish, OrderFinishTable, StateOrder, Status} from '../../models/OrderActive';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {OrderService} from '../../services/order.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';


const ELEMENT_DATA: OrderFinish[] = [];

@Component({
  selector: 'app-complete-order',
  templateUrl: './complete-order.component.html',
  styleUrls: ['./complete-order.component.css']
})
export class CompleteOrderComponent implements OnInit, AfterViewInit {

  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  private order = 'orders.date_create';
  private direction = 'desc';
  private data: any;
  private orderTable: OrderFinishTable;

  constructor(private mainComponent: MainComponent, private orderService: OrderService, private route: ActivatedRoute,
              private router: Router) {
    this.data = this.route.snapshot.data.data;
    console.log(this.data);
  }

  displayedColumns: string[] = ['number_order', 'date_finish', 'addressDepart', 'addressDest', 'driver', 'client', 'sum', 'status'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  // MatPaginator Inputs
  length = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10];

  requestString = '';
  mapSortKeys: Map<string, string>;
  success = Status.SUCCESS;

  ngOnInit() {
    this.orderTable = this.orderService.getOrdersFinishTable(this.data);
    this.refreshTable(this.orderTable);
    this.mainComponent.initActiveNode('Завершенные заказы', '/call-center/working/complete-orders');
  }

  ngAfterViewInit(): void {
    this.mapSortKeys = new Map();
    this.dataSource.sort = this.sort;
    this.sort.disableClear = true;
    this.mapSortKeys.set('number_order', 'orders.id').set('date_finish', 'orders.data_start_finding');
    this.sort.sortChange.subscribe(() => {
      if (this.sort.direction !== '') {
        this.order = this.mapSortKeys.get(this.sort.active);
        this.direction = this.sort.direction;
        this.paginator.pageIndex = 0;
        this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
          '0');
      }
    });
    this.paginator.page.subscribe(() => {
      this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
        (this.paginator.pageSize * this.paginator.pageIndex).toString());
    });
  }

  request(filter: string, order: string, direction: string, limit: string, offset: string) {
    this.orderService.getOrdersFinished(filter, order, direction, limit, offset, StateOrder.FINISHED, result => {
      this.orderTable = result as OrderFinishTable;
      this.refreshTable(this.orderTable);
    });
  }

  refreshTable(orderTable: OrderFinishTable) {
    this.dataSource = new MatTableDataSource(orderTable.orders);
    this.dataSource.sort = this.sort;
    this.length = orderTable.length;
  }


  applyFilter(filterValue: string) {
    if (this.requestString !== filterValue.trim()) {
      this.requestString = filterValue.trim();
      this.request(filterValue.trim(), this.order, 'desc', this.paginator.pageSize.toString(),
        '0');
    }
  }

  selectRow(numberOrder: string) {
    this.router.navigate(['main/call-center/working/order-info/' + numberOrder]);
  }
}
