import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MainComponent} from '../main/main.component';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {DriversService} from '../../services/drivers.service';
import {Driver, DriverTable} from '../../models/driver';
import {ActivatedRoute} from '@angular/router';


const ELEMENT_DATA: Driver[] = [];

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.css']
})


export class DriversComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  private order = 'person.id';
  private direction = 'desc';
  private data: any;
  private driverTable: DriverTable;

  constructor(private mainComponent: MainComponent, private driverService: DriversService, private route: ActivatedRoute) {
    this.data = this.route.snapshot.data.data;
  }

  displayedColumns: string[] = ['id', 'name', 'phone', 'balance', 'auto', 'isBlock'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  // MatPaginator Inputs
  length = 0;
  pageSize = 15;
  pageSizeOptions: number[] = [5, 10, 15];

  requestString = '';
  mapSortKeys: Map<string, string>;

  ngOnInit() {
    this.driverTable = this.driverService.getDriverTable(this.data);
    this.refreshTable(this.driverTable);
    this.mainComponent.initActiveNode('Водители', 'main/database/drivers');
  }

  ngAfterViewInit(): void {
    this.mapSortKeys = new Map();
    this.dataSource.sort = this.sort;
    this.sort.disableClear = true;
    this.mapSortKeys.set('id', 'person.id').set('name', 'first_name').set('phone', 'phone_number')
      .set('balance', 'balance').set('auto', 'number').set('isBlock', 'black_list.id');
    this.sort.sortChange.subscribe(() => {
      if (this.sort.direction !== '') {
        this.order = this.mapSortKeys.get(this.sort.active);
        this.direction = this.sort.direction;
        this.paginator.pageIndex = 0;
        this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
          '0');
      }
    });
    this.paginator.page.subscribe(() => {
      this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
        (this.paginator.pageSize * this.paginator.pageIndex).toString());
    });
  }

  request(filter: string, order: string, direction: string, limit: string, offset: string) {
    this.driverService.getDrivers(filter, order, direction, limit, offset, result => {
      this.driverTable = result as DriverTable;
      this.refreshTable(this.driverTable);
    });
  }

  refreshTable(driverTable: DriverTable) {
    this.dataSource = new MatTableDataSource(driverTable.drivers);
    this.dataSource.sort = this.sort;
    this.length = driverTable.length;
  }


  applyFilter(filterValue: string) {
    if (this.requestString !== filterValue.trim()) {
      this.requestString = filterValue.trim();
      this.request(filterValue.trim(), 'person.id', 'desc', this.paginator.pageSize.toString(),
        '0');
    }
  }
}
