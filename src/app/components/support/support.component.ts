import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MainComponent} from '../main/main.component';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {Client, ClientTable} from '../../models/client';
import {ClientsService} from '../../services/clients.service';
import {ActivatedRoute} from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import {Support} from '../../models/support';


declare const navigator: any;
declare const MediaRecorder: any;

const ELEMENT_DATA: Support[] = [{
  id: '1',
  date: '30.12.2019 04:03:47',
  email: 'ilya.dolgushev1998@gmail.com',
  message: 'Забыл телефон в автомобиле вчера'
}, {
  id: '2',
  date: '30.12.2019 04:21:47',
  email: 'ilya.dolgushev1998@gmail.com',
  message: 'Не могу заказать такси. Заказываю такси в городе Санкт-Петербург, после чего наблюдаю бесконечный поиск водителя. С чем это может быть связано?'
}];

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['id', 'date', 'email', 'message'];
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  private order = 'person.id';
  private direction = 'desc';
  private readonly data: any;
  private clientTable: ClientTable;

  public isRecording = false;
  private chunks: any = [];
  private mediaRecorder: any;

  constructor(private mainComponent: MainComponent, private clientService: ClientsService, private route: ActivatedRoute) {
    // this.data = this.route.snapshot.data.data;
    const onSuccess = stream => {
      this.mediaRecorder = new MediaRecorder(stream);
      this.mediaRecorder.onstop = e => {
        const audio = new Audio();
        const blob = new Blob(this.chunks, {type: 'audio/ogg; codecs=opus'});
        this.chunks.length = 0;
        audio.src = window.URL.createObjectURL(blob);
        audio.load();
        audio.play();
      };

      this.mediaRecorder.ondataavailable = e => this.chunks.push(e.data);
    };

    navigator.getUserMedia = (navigator.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia ||
      navigator.msGetUserMedia);

    navigator.getUserMedia({audio: true}, onSuccess, e => console.log(e));
  }

  dataSource = new MatTableDataSource(ELEMENT_DATA);
  length = 0;
  pageSize = 15;
  pageSizeOptions: number[] = [5, 10, 15];

  requestString = '';
  mapSortKeys: Map<string, string>;

  ngOnInit() {
    // this.clientTable = this.clientService.getClientTable(this.data);
    // this.refreshTable(this.clientTable);
    this.mainComponent.initActiveNode('Техническая поддержка', 'main/call-center/working/support');
  }


  ngAfterViewInit(): void {
    // this.mapSortKeys = new Map();
    this.dataSource.sort = this.sort;
    this.sort.disableClear = true;
    // this.mapSortKeys.set('id', 'person.id').set('name', 'first_name').set('isBlock', 'black_list.id');
    // this.initSortListener();
    // this.initPageListener();
  }

  initSortListener() {
    this.sort.sortChange.subscribe(() => {
      if (this.sort.direction !== '') {
        this.order = this.mapSortKeys.get(this.sort.active);
        this.direction = this.sort.direction;
        this.paginator.pageIndex = 0;
        this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
          '0');
      }
    });
  }

  initPageListener() {
    this.paginator.page.subscribe(() => {
      this.request(this.requestString, this.order, this.sort.direction, this.paginator.pageSize.toString(),
        (this.paginator.pageSize * this.paginator.pageIndex).toString());
    });
  }

  filterEvent(filterValue: string) {
    if (this.requestString !== filterValue.trim()) {
      this.requestString = filterValue.trim();
      this.request(filterValue.trim(), 'person.id', 'desc', this.paginator.pageSize.toString(),
        '0');
    }
  }

  request(filter: string, order: string, direction: string, limit: string, offset: string) {
    this.clientService.getClients(filter, order, direction, limit, offset, result => {
      this.clientTable = result as ClientTable;
      this.refreshTable(this.clientTable);
    });
  }

  refreshTable(clientTable: ClientTable) {
    // this.dataSource = new MatTableDataSource(clientTable.clients);
    // this.dataSource.sort = this.sort;
    // this.length = clientTable.length;
  }

  public record() {
    this.isRecording = true;
    this.mediaRecorder.start();
  }

  public stop() {
    this.isRecording = false;
    this.mediaRecorder.stop();
  }

}


