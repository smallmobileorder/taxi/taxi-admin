import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth.service';
import {Observable, of} from 'rxjs';
import {MAIN_URL} from '../app.router';
import {OrderActiveTable, OrderFinishTable, OrderInfo, StateOrder, Status} from '../models/OrderActive';
import {delay} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private httpClient: HttpClient, private  authService: AuthService) {
  }

  public getOrdersObservable(filter: string, order: string, direction: string, limit: string, offset: string, state: StateOrder): Observable<any> {
    const url = MAIN_URL + '/query/getOrdersByState';
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded',
        filter,
        order,
        direction,
        limit,
        offset,
        state
      })
    };
    return this.httpClient.get<any>(url, httpOptions);
  }

  public getOrdersActive(filter: string, order: string, direction: string, limit: string, offset: string, state: StateOrder, callback: any) {
    this.getOrdersObservable(filter, order, direction, limit, offset, state).subscribe(value => {
        console.log(value);
        const orderTable = this.getOrdersActiveTable(value);
        callback(orderTable);
      },
      error => {
        console.log(error);
        this.authService.refresh();
        callback(null);
      });
  }

  public getOrdersFinished(filter: string, order: string, direction: string, limit: string, offset: string, state: StateOrder, callback: any) {
    this.getOrdersObservable(filter, order, direction, limit, offset, state).subscribe(value => {
        console.log(value);
        const orderTable = this.getOrdersFinishTable(value);
        callback(orderTable);
      },
      error => {
        console.log(error);
        this.authService.refresh();
        callback(null);
      });
  }

  getOrdersActiveTable(data: any): OrderActiveTable {
    console.log(data);
    const dataResponse = {
      ordersFinding: [],
      ordersWaiting: [],
      ordersTrip: [],
    };
    data.forEach(orders => {
      orders.forEach(item => {
        console.log(item);
        let strBuilder = '';
        if (item.last_name !== 'empty') {
          strBuilder = strBuilder + item.last_name;
        }
        if (item.first_name !== 'empty') {
          strBuilder = strBuilder + item.first_name;
        }
        if (strBuilder === '') {
          strBuilder = 'Аноним';
        }
        const order = {
          number_order: item.orders_id,
          timer: new Date(item.data_start_finding).toLocaleDateString() + ' ' + new Date(item.data_start_finding).toLocaleTimeString(),
          addressDepart: item.start_address,
          addressDest: item.end_address,
          driver: item.pd_first_name === undefined ? '' : item.pd_first_name + ' ' + item.pd_last_name,
          phone: item.pc_phone_number,
          phoneDriver: item.pd_phone_number,
          client: strBuilder,
          state: item.state
        };
        if (order.state === StateOrder.WAITING) {
          dataResponse.ordersWaiting.push(order);
        } else if (order.state === StateOrder.TRIP) {
          dataResponse.ordersTrip.push(order);
        } else if (order.state === StateOrder.FINDING) {
          dataResponse.ordersFinding.push(order);
        }
      });
    });
    return dataResponse as OrderActiveTable;
  }

  getOrdersFinishTable(data: any): OrderFinishTable {
    const dataResponse = {
      orders: [],
      length: 0
    };

    data.forEach(item => {
      let strBuilder = '';
      if (item.last_name !== 'empty') {
        strBuilder = strBuilder + item.last_name;
      }
      if (item.first_name !== 'empty') {
        strBuilder = strBuilder + item.first_name;
      }
      if (strBuilder === '') {
        strBuilder = 'Аноним';
      }
      let status = Status.CANCELED_DRIVER;
      if (item.status === 'SUCCESS') {
        status = Status.SUCCESS;
      } else {
        if (item.status === 'CLIENT_CANCELED') {
          status = Status.CANCELED_CLIENT;
        }
      }
      dataResponse.orders.push({
        number_order: item.orders_id,
        date_finish: new Date(item.data_finish_trip).toLocaleDateString() + ' ' + new Date(item.data_finish_trip).toLocaleTimeString(),
        date_start: new Date(item.data_start_trip).toLocaleDateString() + ' ' + new Date(item.data_start_trip).toLocaleTimeString(),
        addressDepart: item.start_address,
        addressDest: item.end_address,
        phone: item.pc_phone_number,
        driver: item.pd_first_name + ' ' + item.pd_last_name,
        client: strBuilder,
        status,
        sum: item.price
      });
      dataResponse.length = item.full_count;
    });
    return dataResponse as OrderFinishTable;
  }

  getOrderObservable(id: string) {
    const url = MAIN_URL + '/query/getOrder';
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded',
        'id-order': id
      })
    };
    return this.httpClient.get<any>(url, httpOptions);
  }

  getOrderInfo(data: any): OrderInfo {
    console.log(data);
    const dataResponse = {
      id_order: '',
      state_order: '',
      datetime_start: '',
      datetime_finish: '',
      phone_client: '',
      phone_driver: '',
      fio_client: '',
      fio_driver: '',
      address_depart: '',
      address_dest: '',
      sum: ''
    };
    dataResponse.id_order = data.id;
    dataResponse.state_order = data.id;
    dataResponse.datetime_start = new Date(data.data_start_finding).toLocaleDateString() + ' ' + new Date(data.data_start_finding).toLocaleTimeString();
    dataResponse.datetime_finish = new Date(data.data_finish_trip).toLocaleDateString() + ' ' + new Date(data.data_finish_trip).toLocaleTimeString();
    dataResponse.phone_client = data.pc_phone_number;
    dataResponse.phone_driver = data.pd_phone_number;
    dataResponse.fio_client = '';
    dataResponse.fio_driver = data.pd_first_name + ' ' + data.pd_last_name + ' ' + data.patronymic;
    dataResponse.address_depart = data.start_address;
    dataResponse.address_dest = data.end_address;
    dataResponse.sum = data.price;
    return dataResponse as OrderInfo;
  }

  startGettingData() {

  }

}

