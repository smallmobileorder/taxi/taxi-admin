import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs';
import {MAIN_URL} from '../app.router';
import {Config, DriversTariffsConfig, DriversTariffsConfigs} from '../models/config';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(private httpClient: HttpClient, private  authService: AuthService) {
  }

  getMainConfigObservable(): Observable<any> {
    const url = MAIN_URL + '/query/getMainConfigs';
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded',
      })
    };
    return this.httpClient.get(url, httpOptions);
  }

  public getMainConfig(callBack: any) {
    this.getMainConfigObservable().subscribe(value => {
        const mainConfigs = value as Config;
        callBack(mainConfigs);
      },
      error => {
        console.log(error);
        this.authService.refresh();
        callBack(null);
      });
  }

  getMainConfigData(data: any): Config {
    const dataResponse = {
      min_balance_driver: '',
      max_count_halt: '',
      time_free_waiting: '',
      time_for_cancel_order: '',
      price_for_min_waiting: '',
      price_for_halt: '',
      price_for_halt_in_way: '',
      time_for_cancel_order_without_driver: '',
      time_for_finish_order: '',
      time_for_finish_order_without_rating: '',
      life_time_rates: '',
      min_balance_driver_for_accept_order: '',
      time_for_check_status_call_client: '',
      interval_for_check_status_call_client: '',
      count_for_check_status_call_client: '',
      cancel_order_without_call: '',
      call_for_up_price: '',
      commission_for_fine: '',
      amount_fine: '',
      id_city: 1,
    };
    dataResponse.min_balance_driver = data.rows[0].min_balance_driver === undefined ? '' : data.rows[0].min_balance_driver;
    dataResponse.max_count_halt = data.rows[0].max_count_halt === undefined ? '' : data.rows[0].max_count_halt;
    dataResponse.time_free_waiting = data.rows[0].time_free_waiting === undefined ? '' : data.rows[0].time_free_waiting;
    dataResponse.time_for_cancel_order = data.rows[0].time_for_cancel_order === undefined ? '' : data.rows[0].time_for_cancel_order;
    dataResponse.price_for_min_waiting = data.rows[0].price_for_min_waiting === undefined ? '' : data.rows[0].price_for_min_waiting;
    dataResponse.price_for_halt = data.rows[0].price_for_halt === undefined ? '' : data.rows[0].price_for_halt;
    dataResponse.price_for_halt_in_way = data.rows[0].price_for_halt_in_way === undefined ? '' : data.rows[0].price_for_halt_in_way;
    dataResponse.time_for_cancel_order_without_driver = data.rows[0].time_for_cancel_order_without_driver === undefined ? '' : data.rows[0].time_for_cancel_order_without_driver;
    dataResponse.time_for_finish_order = data.rows[0].time_for_finish_order === undefined ? '' : data.rows[0].time_for_finish_order;
    dataResponse.time_for_finish_order_without_rating = data.rows[0].time_for_finish_order_without_rating === undefined ? '' : data.rows[0].time_for_finish_order_without_rating;
    dataResponse.life_time_rates = data.rows[0].life_time_rates === undefined ? '' : data.rows[0].life_time_rates;
    dataResponse.min_balance_driver_for_accept_order = data.rows[0].min_balance_driver_for_accept_order === undefined ? '' : data.rows[0].min_balance_driver_for_accept_order;
    dataResponse.time_for_check_status_call_client = data.rows[0].time_for_check_status_call_client === undefined ? '' : data.rows[0].time_for_check_status_call_client;
    dataResponse.interval_for_check_status_call_client = data.rows[0].interval_for_check_status_call_client === undefined ? '' : data.rows[0].interval_for_check_status_call_client;
    dataResponse.count_for_check_status_call_client = data.rows[0].count_for_check_status_call_client === undefined ? '' : data.rows[0].count_for_check_status_call_client;
    dataResponse.cancel_order_without_call = data.rows[0].cancel_order_without_call === undefined ? '' : data.rows[0].cancel_order_without_call;
    dataResponse.call_for_up_price = data.rows[0].call_for_up_price === undefined ? '' : data.rows[0].call_for_up_price;
    dataResponse.commission_for_fine = data.rows[0].commission_for_fine === undefined ? '' : data.rows[0].commission_for_fine;
    dataResponse.amount_fine = data.rows[0].amount_fine === undefined ? '' : data.rows[0].amount_fine;
    dataResponse.id_city = data.rows[0].id_city === undefined ? 1 : Number.parseInt(data.rows[0].id_city, 10);

    dataResponse.time_for_cancel_order_without_driver = this.convertToSec(dataResponse.time_for_cancel_order_without_driver);
    dataResponse.time_for_finish_order = this.convertToSec(dataResponse.time_for_finish_order);
    dataResponse.time_for_finish_order_without_rating = this.convertToSec(dataResponse.time_for_finish_order_without_rating);
    dataResponse.time_for_check_status_call_client = this.convertToSec(dataResponse.time_for_check_status_call_client);
    dataResponse.interval_for_check_status_call_client = this.convertToSec(dataResponse.interval_for_check_status_call_client);
    dataResponse.time_for_cancel_order = this.convertToSec(dataResponse.time_for_cancel_order);
    dataResponse.call_for_up_price = this.convertToSec(dataResponse.call_for_up_price);
    return dataResponse as Config;
  }

  updateConfigs(mainConfigs: Config, callback: any) {

    this.updateMainConfigObservable(mainConfigs).subscribe(value => {
        console.log(value);
        callback(true);
      },
      error => {
        console.log(error);
        callback(false);
      });

  }

  updateMainConfigObservable(mainConfigs: Config): Observable<any> {
    const url = MAIN_URL + '/query/updateMainConfig';
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded',
      })
    };
    const body = new URLSearchParams();
    body.append('min_balance_driver', mainConfigs.min_balance_driver);
    body.append('max_count_halt', mainConfigs.max_count_halt);
    body.append('time_free_waiting', mainConfigs.time_free_waiting);
    body.append('time_for_cancel_order', mainConfigs.time_for_cancel_order);
    body.append('price_for_min_waiting', mainConfigs.price_for_min_waiting);
    body.append('price_for_halt', mainConfigs.price_for_halt);
    body.append('price_for_halt_in_way', mainConfigs.price_for_halt_in_way);
    body.append('time_for_cancel_order_without_driver', mainConfigs.time_for_cancel_order_without_driver);
    body.append('time_for_finish_order', mainConfigs.time_for_finish_order);
    body.append('time_for_finish_order_without_rating', mainConfigs.time_for_finish_order_without_rating);
    // body.append('life_time_rates', mainConfigs.life_time_rates);
    body.append('min_balance_driver_for_accept_order', mainConfigs.min_balance_driver_for_accept_order);
    body.append('time_for_check_status_call_client', mainConfigs.time_for_check_status_call_client);
    body.append('interval_for_check_status_call_client', mainConfigs.interval_for_check_status_call_client);
    body.append('count_for_check_status_call_client', mainConfigs.count_for_check_status_call_client);
    // body.append('cancel_order_without_call', mainConfigs.cancel_order_without_call);
    body.append('call_for_up_price', mainConfigs.call_for_up_price);
    body.append('commission_for_fine', mainConfigs.commission_for_fine);
    body.append('id_city', mainConfigs.id_city.toString());
    body.append('amount_fine', mainConfigs.amount_fine);
    body.append('id', '1');
    return this.post(url, body.toString(), httpOptions);
  }

  public post(url: string, data: any, httpOptions): Observable<any> {
    return this.httpClient.post(url, data, httpOptions);
  }

  private convertToSec(time: string): string {
    console.log(time);
    const timeArr = time.split(':');
    console.log(timeArr);
    const result = Number.parseInt(timeArr[0], 10) * 60 * 60 + Number.parseInt(timeArr[1], 10) * 60
      + Number.parseInt(timeArr[2], 10);
    return result.toString();
  }


}
