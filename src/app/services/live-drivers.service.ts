import {Injectable} from '@angular/core';
import {DriverLatLng} from '../models/driver';
import {Observable} from 'rxjs';
import {MAIN_URL} from '../app.router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class LiveDriversService {

  constructor(private httpClient: HttpClient, private  authService: AuthService) {

  }

  getDriverLatLngObservable(): Observable<any> {
    const url = MAIN_URL + '/query/getPositionsAllDriver';
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded'
      })
    };
    return this.httpClient.get<any>(url, httpOptions);
  }

  getDriversWithLatLng(data: any): DriverLatLng[] {
    console.log(data);
    const dataResponse = [];
    data.forEach(item => {
      dataResponse.push({
        id: item.d_id,
        lat: Number(item.last_lat),
        lng: Number(item.last_lng),
        last_angel: Number(item.last_angel),
        phone_number: item.phone_number,
        first_name: item.first_name,
        last_name: item.last_name,
        patronymic: item.patronymic
      });
    });
    return dataResponse as DriverLatLng[];
  }
}
