import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth.service';
import {MAIN_URL} from '../app.router';
import {Observable} from 'rxjs';
import {ClientTable} from '../models/client';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(private httpClient: HttpClient, private  authService: AuthService) {
  }

  public getClientsObservable(filter: string, order: string, direction: string, limit: string, offset: string): Observable<any> {
    const url = MAIN_URL + '/query/getListClients';
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded',
        filter,
        order,
        direction,
        limit,
        offset
      })
    };
    return this.httpClient.get<any>(url, httpOptions);
  }

  getClients(filter: string, order: string, direction: string, limit: string, offset: string, callback: any) {
    this.getClientsObservable(filter, order, direction, limit, offset).subscribe(value => {
        const clientTable = this.getClientTable(value);
        callback(clientTable);
      },
      error => {
        console.log(error);
        this.authService.refresh();
        callback(null);
      });
  }

  getClientTable(data: any): ClientTable {
    const dataResponse = {
      clients: [],
      length: 0
    };
    console.log(data);

    data.rows.forEach(item => {
      let strBuilder = '';
      if (item.last_name !== 'empty') {
        strBuilder = strBuilder + item.last_name;
      }
      if (item.first_name !== 'empty') {
        strBuilder = strBuilder + item.first_name;
      }
      if (strBuilder === '') {
        strBuilder = 'Анонимно';
      }
      dataResponse.clients.push({
        id: item.c_id,
        name: strBuilder,
        phone: item.phone_number,
        isBlock: item.bl_id !== null
      });
      dataResponse.length = item.full_count;
    });
    console.log(data);
    console.log(dataResponse as ClientTable);
    return dataResponse as ClientTable;
  }
}
