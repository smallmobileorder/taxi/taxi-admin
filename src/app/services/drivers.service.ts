import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth.service';
import {MAIN_URL} from '../app.router';
import {Observable} from 'rxjs';
import {DriverTable} from '../models/driver';

@Injectable({
  providedIn: 'root'
})
export class DriversService {

  constructor(private httpClient: HttpClient, private  authService: AuthService) {
  }

  public getDriversObservable(filter: string, order: string, direction: string, limit: string, offset: string): Observable<any> {
    const url = MAIN_URL + '/query/getListDrivers';
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded',
        filter,
        order,
        direction,
        limit,
        offset
      })
    };
    return this.httpClient.get<any>(url, httpOptions);
  }

  public getDrivers(filter: string, order: string, direction: string, limit: string, offset: string, callback: any) {
    this.getDriversObservable(filter, order, direction, limit, offset).subscribe(value => {
        console.log(value);
        const driverTable = this.getDriverTable(value);
        callback(driverTable);
      },
      error => {
        console.log('start Error');
        console.log(error);
        console.log('end Error');
        this.authService.refresh();
        callback(null);
      });
  }

  getDriverTable(data: any): DriverTable {
    const dataResponse = {
      drivers: [],
      length: 0
    };
    data.rows.forEach(item => {
      dataResponse.drivers.push({
        id: item.d_id,
        name: item.last_name + ' ' + item.first_name + ' ' + (item.patronymic === null ? '' : item.patronymic),
        phone: item.phone_number,
        balance: item.balance,
        auto: item.car_number,
        isBlock: item.bl_id !== null
      });
      dataResponse.length = item.full_count;
    });
    return dataResponse as DriverTable;
  }
}
