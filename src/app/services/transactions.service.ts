import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth.service';
import {MAIN_URL} from '../app.router';
import {Observable} from 'rxjs';
import {TransactionTable} from '../models/transaction';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  constructor(private httpClient: HttpClient, private  authService: AuthService) {
  }

  public getTransaction(filter: string, order: string, direction: string, limit: string, offset: string, callback: any) {
    this.getTransactionObservable(filter, order, direction, limit, offset).subscribe(value => {
        const dataResponse = this.getTransactionTable(value);
        callback(dataResponse);
      },
      error => {
        console.log(error);
        this.authService.refresh();
        callback(null);
      });
  }

  public getTransactionObservable(filter: string, order: string, direction: string, limit: string, offset: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded',
        filter,
        order,
        direction,
        limit,
        offset
      })
    };
    const url = MAIN_URL + '/query/getTransaction';
    return this.httpClient.get<any>(url, httpOptions);
  }

  getTransactionTable(data: any): TransactionTable {
    const typeOperation = 'Зачисление баланса';
    const typeObject = 'Внутренний счет';
    const typeBalance = 'Баланс водителя';
    const dataResponse = {
      transactions: [],
      length: 0
    };
    // @ts-ignore
    // tslint:disable-next-line:only-arrow-functions
    data.rows.forEach(function (item) {
      dataResponse.transactions.push({
        id: item.ps_id,
        date: new Date(item.timestamp).toLocaleDateString(),
        balance_before: item.balance_before,
        sum: item.amount,
        balance_after: Number(item.balance_before) + Number(item.amount),
        type_operation: typeOperation,
        payment_system: item.payment_system,
        type_object: typeObject,
        type_balance: typeBalance,
        comment: item.comment,
        phone: item.phone_number,
      });
      dataResponse.length = item.full_count;
    });
    return dataResponse as TransactionTable;
  }
}
