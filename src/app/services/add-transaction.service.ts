import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs';
import {MAIN_URL} from '../app.router';

@Injectable({
  providedIn: 'root'
})
export class AddTransactionService {

  constructor(private httpClient: HttpClient, private  authService: AuthService) {
  }

  addTransaction(date: string, idDriver: string, sumAdd: string, typeOperation: string, paymentSystem: string, typeObject: string,
                 typeBalance: string, commentValue: string, callback: any) {
    this.addTransactionObservable(date, idDriver, sumAdd, typeOperation, paymentSystem, typeObject, typeBalance, commentValue).subscribe(value => {
      console.log(value);
      callback(true);
    }, error => {
      console.log(error);
      this.authService.refresh();
      callback(false);
    });
  }

  convertDateTime(dateTimeString: string): string {
    const dateTimeArr = dateTimeString.split('.');
    return dateTimeArr[0] + ':' + dateTimeArr[1] + ':' + dateTimeArr[2] + ' 00:00:00';
  }

  addTransactionObservable(date: string, idDriver: string, sumAdd: string, typeOperation: string, paymentSystem: string, typeObject: string, typeBalance: string, commentValue: string): Observable<any> {

    const url = MAIN_URL + '/query/insertTransaction';
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded',
      })
    };
    console.log(date);
    console.log(this.getDate(date));
    const body = new URLSearchParams();
    body.append('timestamp', this.getDate(date));
    body.append('amount', sumAdd);
    body.append('id_driver', idDriver);
    body.append('type_operation', typeOperation);
    body.append('payment_system', paymentSystem);
    body.append('type_object', typeObject);
    body.append('type_balance', typeBalance);
    body.append('comment', commentValue);

    return this.post(url, body.toString(), httpOptions);
  }


  public getTime(time: string): string {
    const timeArr = time.split(' ');
    const date = timeArr[0];
    const dateArr = date.split('.');
    const newDate = dateArr[2] + '-' + dateArr[1] + '-' + dateArr[0];
    return newDate + ' ' + timeArr[1];
  }

  public getDate(date: string): string {
    const dateArr = date.split('-');
    return dateArr[2] + '-' + dateArr[1] + '-' + dateArr[0];
  }

  public post(url: string, data: any, httpOptions): Observable<any> {
    return this.httpClient.post(url, data, httpOptions);
  }
}
