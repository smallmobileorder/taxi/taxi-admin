import {Injectable} from '@angular/core';
import {User} from '../models/user';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {MAIN_URL, routes} from '../app.router';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient, private router: Router) {
  }


  public login(userInfo: User, callback: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded'
      })
    };
    console.log('post');
    const body = new URLSearchParams();
    body.append('username', userInfo.email);
    body.append('password', userInfo.password);
    body.append('grant_type', 'password');
    body.append('client_id', null);
    body.append('client_secret', null);
    const url = MAIN_URL + '/auth/login';
    console.log(body);
    this.post(url, body.toString(), httpOptions).subscribe(value => {
        console.log(value);
        const tokenAccess = value.access_token;
        const tokenRefresh = value.refresh_token;
        if (tokenAccess != null) {
          localStorage.setItem('ACCESS_TOKEN', tokenAccess);
          localStorage.setItem('REFRESH_TOKEN', tokenRefresh);
          callback(true);
        } else {
          callback(false);
        }
      },
      error => {
        console.log(error);
        callback(false);
      });
  }

  public post(url: string, data: any, httpOptions): Observable<any> {
    return this.httpClient.post(url, data, httpOptions);
  }

  public isLoggedIn() {
    return localStorage.getItem('ACCESS_TOKEN') !== null;
  }

  public logout() {
    localStorage.removeItem('ACCESS_TOKEN');
    localStorage.removeItem('REFRESH_TOKEN');
    this.router.navigateByUrl('/login');
  }

  public refresh() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded'
      })
    };
    const urlRefresh = MAIN_URL + '/auth/refresh';
    const body = new URLSearchParams();
    body.append('refresh_token', localStorage.getItem('REFRESH_TOKEN'));
    body.append('grant_type', 'refresh_token');
    body.append('client_id', null);
    body.append('client_secret', null);
    this.post(urlRefresh, body.toString(), httpOptions).subscribe(value => {
        const tokenAccess = value.access_token;
        // @ts-ignore
        const tokenRefresh = value.refresh_token;
        if (tokenAccess != null) {
          localStorage.setItem('ACCESS_TOKEN', tokenAccess);
          localStorage.setItem('REFRESH_TOKEN', tokenRefresh);
        }
        this.router.navigate(['main/drivers/registration-internal']);
      },
      error => {
        this.logout();
      });
  }
}
