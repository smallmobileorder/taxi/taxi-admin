import {Injectable} from '@angular/core';
import {MAIN_URL} from '../app.router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterUserService {

  constructor(private httpClient: HttpClient, private  authService: AuthService) {
  }

  public addDriver(firstNameValue: string, secondNameValue: string, patronymicValue: string, birthdayValue: string, addressValue: string, addressValue2: string, phoneValue: string,
                   emailValue: string, seriaPasportValue: string, numberPasportValue: string, datePasportValue: string,
                   seriaVodValue: string, numberVodValue: string, dateVodValue: string, dateAccessValue: string, markValue: string,
                   modelValue: string, colorValue: string, numberValue: string, yearValue: string, countDoorValue: string,
                   vinValue: string, commentValue: string, gender: boolean, timeWriting: string, dateWriting: string, callback: any) {
    this.addDriverObservable(firstNameValue, secondNameValue, patronymicValue, birthdayValue, addressValue, addressValue2, phoneValue,
      emailValue, seriaPasportValue, numberPasportValue, datePasportValue,
      seriaVodValue, numberVodValue, dateVodValue, dateAccessValue, markValue,
      modelValue, colorValue, numberValue, yearValue, countDoorValue,
      vinValue, commentValue, gender, timeWriting, dateWriting).subscribe(value => {
      console.log(value);
      callback(true);
    }, error => {
      console.log(error);
      this.authService.refresh();
      callback(false);
    });
  }

  addDriverObservable(firstNameValue: string, secondNameValue: string, patronymicValue: string, birthdayValue: string,
                      addressValue: string, addressValue2: string, phoneValue: string, emailValue: string,
                      seriaPasportValue: string, numberPasportValue: string, datePasportValue: string,
                      seriaVodValue: string, numberVodValue: string, dateVodValue: string, dateAccessValue: string,
                      markValue: string, modelValue: string, colorValue: string, numberValue: string, yearValue: string,
                      countDoorValue: string, vinValue: string, commentValue: string, gender: boolean,
                      timeWriting: string, dateWriting: string): Observable<any> {
    const time = timeWriting + ':00';
    const result = dateWriting + ' ' + time;
    console.log('Номер телефона: ');
    console.log('+7' + phoneValue);
    const url = MAIN_URL + '/query/insertFullRequestForDrivers';
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded',
      })
    };
    console.log(dateVodValue);
    console.log(this.getDate(dateVodValue));
    console.log(result);
    console.log(this.getTime(result));
    const body = new URLSearchParams();
    body.append('model_name', modelValue);
    body.append('car_number', numberValue);
    body.append('email', emailValue);
    body.append('mark_name', markValue);
    body.append('vin_value', vinValue);
    body.append('color', colorValue);
    body.append('year_made', yearValue);
    body.append('photo_register_back', null);
    body.append('photo_register_front', null);
    body.append('count_doors', countDoorValue);
    body.append('license_front', null);
    body.append('license_back', null);
    body.append('serial_license', seriaVodValue);
    body.append('number_license', numberVodValue);
    body.append('date_register_license', this.getDate(dateVodValue));
    body.append('date_end_license', this.getDate(dateAccessValue));
    body.append('address_value_fact', addressValue);
    body.append('address_value_register', addressValue2);
    body.append('serial_passport', seriaPasportValue);
    body.append('number_passport', numberPasportValue);
    body.append('date_register_passport', this.getDate(datePasportValue));
    body.append('photo_passport_main', null);
    body.append('photo_passport_register', null);
    body.append('first_name', firstNameValue);
    body.append('last_name', secondNameValue);
    body.append('birthday_value', this.getDate(birthdayValue));
    body.append('gender', String(gender));
    body.append('patronymic', patronymicValue);
    body.append('phone_number', '+7' + phoneValue);
    body.append('balance', '0');
    body.append('comment', commentValue);
    body.append('date_status_change', this.getTime(result));
    body.append('date_create', this.getTime(result));
    body.append('state', 'WAITING');

    return this.post(url, body.toString(), httpOptions);
  }

  public getTime(time: string): string {
    const timeArr = time.split(' ');
    const date = timeArr[0];
    const dateArr = date.split('.');
    const newDate = dateArr[2] + '-' + dateArr[1] + '-' + dateArr[0];
    return newDate + ' ' + timeArr[1];
  }

  public getDate(date: string): string {
    const dateArr = date.split('.');
    return dateArr[2] + '-' + dateArr[1] + '-' + dateArr[0];
  }

  public post(url: string, data: any, httpOptions): Observable<any> {
    return this.httpClient.post(url, data, httpOptions);
  }
}
