import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth.service';
import {MAIN_URL} from '../app.router';
import {AccountApplication, AccountApplicationShortTable} from '../models/accountApplication';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountApplicationService {


  constructor(private httpClient: HttpClient, private  authService: AuthService) {
  }

  getAccountApplicationObservable(id: string): Observable<any> {
    const url = MAIN_URL + '/query/getRequestForDriver';
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded',
        id
      })
    };
    return this.httpClient.get(url, httpOptions);
  }

  public getAccountApplication(id: string, callBack: any) {

    this.getAccountApplicationObservable(id).subscribe(value => {
        const accountApplication = value as AccountApplication;
        callBack(accountApplication);
      },
      error => {
        console.log(error);
        this.authService.refresh();
        callBack(null);
      });
  }

  getAccountApplicationData(data: any): AccountApplication {
    console.log(data);
    const dataResponse = {
      id: 0,
      first_name: '1',
      second_name: '1',
      patronymic: '1',
      gender: true,
      date_birthday: '1',
      address1: '1',
      address2: '1',
      phone: '1',
      email: '1',
      seria_pasport: '1',
      number_pasport: '1',
      date_vidachi_pasport: '1',
      seria_voditelskoye: '1',
      number_voditelskoye: '1',
      date_vidachi_voditelskoye: '1',
      date_access_voditelskoye: '1',
      mark: '1',
      model: '1',
      color: '1',
      number: '1',
      year_making: '1',
      count_of_doors: '1',
      vin: '1',
      comment: '1',
      status: '1',
      date_writing: '1',
      date_update: '1'
    };
    console.log('Номер телефона который пришел:');
    console.log(data.rows[0].phone_number);
    dataResponse.id = data.rows[0].id;
    dataResponse.first_name = data.rows[0].first_name;
    dataResponse.second_name = data.rows[0].last_name;
    dataResponse.patronymic = data.rows[0].patronymic;
    dataResponse.gender = data.rows[0].gender;
    dataResponse.date_birthday = data.rows[0].birthday_value;
    dataResponse.address1 = data.rows[0].address_value_fact;
    dataResponse.address2 = data.rows[0].address_value_register;
    dataResponse.phone = data.rows[0].phone_number;
    dataResponse.email = data.rows[0].email;
    dataResponse.seria_pasport = data.rows[0].serial_passport;
    dataResponse.number_pasport = data.rows[0].number_passport;
    dataResponse.date_vidachi_pasport = data.rows[0].date_register_passport;
    dataResponse.seria_voditelskoye = data.rows[0].serial_license;
    dataResponse.number_voditelskoye = data.rows[0].number_license;
    dataResponse.date_vidachi_voditelskoye = data.rows[0].date_register_license;
    dataResponse.date_access_voditelskoye = data.rows[0].date_register_license;
    dataResponse.mark = data.rows[0].mark_name;
    dataResponse.model = data.rows[0].model_name;
    dataResponse.color = data.rows[0].color;
    dataResponse.number = data.rows[0].car_number;
    dataResponse.year_making = data.rows[0].year_made;
    dataResponse.count_of_doors = data.rows[0].count_doors;
    dataResponse.vin = data.rows[0].vin_value;
    dataResponse.comment = data.rows[0].comment;
    dataResponse.status = data.rows[0].state;
    dataResponse.date_writing = data.rows[0].date_create;
    dataResponse.date_update = data.rows[0].date_status_change;
    return dataResponse as AccountApplication;
  }

  getModerationList(filter: string, order: string, direction: string, limit: string, offset: string, callback: any) {
    this.getModerationListObservable(filter, order, direction, limit, offset).subscribe(value => {
        const accountDataShort = this.getAccountApplicationDataShort(value);
        callback(accountDataShort);
      },
      error => {
        this.authService.refresh();
        callback(null);
      });
  }

  getModerationListObservable(filter: string, order: string, direction: string, limit: string, offset: string): Observable<any> {
    const url = MAIN_URL + '/query/getRequestForDrivers';
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded',
        filter,
        order,
        direction,
        limit,
        offset
      })
    };
    return this.httpClient.get(url, httpOptions);
  }

  public post(url: string, data: any, httpOptions): Observable<any> {
    return this.httpClient.post(url, data, httpOptions);
  }

  getAccountApplicationDataShort(data: any): AccountApplicationShortTable {
    const dataResponse = {
      accounts: [],
      length: 0
    };
    data.rows.forEach(item => {
      let statusTxt = 'REJECTED';
      let state = 1;
      switch (item.state) {
        case 'REJECTED': {
          statusTxt = 'Отклонен';
          state = 1;
          break;
        }
        case 'ACCEPTED': {
          statusTxt = 'Подтвержден';
          state = 2;
          break;
        }
        default: {
          statusTxt = 'Требует модерации';
          state = 3;
          break;
        }
      }

      dataResponse.accounts.push({
        id: item.id_drivers,
        fio: item.last_name + ' ' + item.first_name + ' ' + (item.patronymic === null ? '' : item.patronymic),
        phone: item.phone_number,
        date_update: new Date(item.date_status_change).toLocaleDateString(),
        date_writing: new Date(item.date_create).toLocaleDateString(),
        status: state,
        status_text: statusTxt
      });
      dataResponse.length = item.full_count;
    });
    return dataResponse as AccountApplicationShortTable;
  }

  // updateRequestState(id: string, accept: boolean, callback: any) {
  //   this.updateStateObservable(id, accept).subscribe(value => {
  //       callback(true);
  //     },
  //     error => {
  //       callback(false);
  //     });
  // }

  private updateStateObservable(id: string, accept: boolean, commentValue: string): Observable<any> {
    const url = MAIN_URL + '/query/updateRequestForDriver';
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded',
      })
    };
    let state = 'WAITING';
    if (accept) {
      state = 'ACCEPTED';
    } else {
      state = 'REJECTED';
    }
    const dateUpdate = new Date().toLocaleDateString();
    const body = new URLSearchParams();
    body.append('id', id);
    body.append('comment', commentValue);
    body.append('date_status_change', this.getTime(dateUpdate));
    body.append('state', state);
    return this.post(url, body.toString(), httpOptions);
  }

  public getTime(time: string): string {
    const timeArr = time.split(' ');
    const date = timeArr[0];
    const dateArr = date.split('.');
    const newDate = dateArr[2] + '-' + dateArr[1] + '-' + dateArr[0];
    return newDate + ' ' + timeArr[1];
  }

  public getDate(date: string): string {
    const dateArr = date.split('.');
    return dateArr[2] + '-' + dateArr[1] + '-' + dateArr[0];
  }

  updateRequestState(idDriver: string, state: boolean, commentValue: string, callback: any) {
    this.updateStateObservable(idDriver, state, commentValue).subscribe(value => {
        callback(true);
      },
      error => {
        callback(false);
      });
  }
}
