import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {MAIN_URL} from '../app.router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {DriversTariffsConfigs} from '../models/config';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TariffsConfigService {

  constructor(private httpClient: HttpClient, private  authService: AuthService) {
  }


  public getDriversTariffsConfigsObservable(filter: string, order: string, direction: string, limit: string,
                                            offset: string): Observable<any> {
    const url = MAIN_URL + '/query/getDriversTariffs';
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'content-type': 'application/x-www-form-urlencoded',
        filter,
        order,
        direction,
        limit,
        offset
      })
    };
    return this.httpClient.get(url, httpOptions);
  }

  getDriversTariffsConfigs(data: any): DriversTariffsConfigs {
    const dataResponse = {
      tariffs: []
    };
    dataResponse.tariffs = data;
    // data.forEach(item => {
    //   console.log(item.name_tariff);
    //   console.log(item.min_price);
    //   console.log(dataResponse.tariffs);
    //   dataResponse.tariffs.push({
    //     id: item.id === undefined ? '' : item.id,
    //     name_tariff: item.name_tariff === undefined ? '' : item.name_tariff,
    //     min_price: item.min_price === undefined ? '' : item.min_price,
    //     price_for_meter: item.price_for_meter === undefined ? '' : item.price_for_meter,
    //     price_for_minute: item.price_for_minute === undefined ? '' : item.price_for_minute,
    //     city_id: item.city_id === undefined ? '' : item.city_id
    //   });
    //   console.log(dataResponse.tariffs);
    // });
    const result = dataResponse as DriversTariffsConfigs;
    console.log(result.tariffs);
    return dataResponse as DriversTariffsConfigs;
  }
}
