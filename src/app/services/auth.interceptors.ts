import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable()
export class AuthInterceptors implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const idToken = localStorage.getItem('ACCESS_TOKEN');
    if (idToken) {
      const clonedRequest = req.clone({headers: req.headers.set('Authorization', 'Bearer ' + localStorage.getItem('ACCESS_TOKEN'))});
      return next.handle(clonedRequest);

    } else {
      return next.handle(req);
    }
  }
}
