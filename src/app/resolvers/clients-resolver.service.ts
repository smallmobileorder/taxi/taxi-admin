import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../services/auth.service';
import {ClientsService} from '../services/clients.service';

@Injectable({
  providedIn: 'root'
})
export class ClientsResolver implements Resolve<any> {

  constructor(private clientsService: ClientsService, private authService: AuthService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

    return this.clientsService.getClientsObservable('', 'person.id', 'desc', '15',
      '0').pipe(catchError(() => {
      this.authService.refresh();
      return EMPTY;
    }));
  }
}

