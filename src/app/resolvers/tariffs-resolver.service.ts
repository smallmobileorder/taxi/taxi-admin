import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {EMPTY, Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../services/auth.service';
import {TariffsConfigService} from '../services/tariffs-config.service';

@Injectable({
  providedIn: 'root'
})
export class TariffsResolverService implements Resolve<any> {

  constructor(private tariffsConfigService: TariffsConfigService, private authService: AuthService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.tariffsConfigService.getDriversTariffsConfigsObservable('', 'tariffs.name_tariff', 'desc', '15',
      '0').pipe(
      catchError(() => {
        this.authService.refresh();
        return EMPTY;
      }));
  }
}

