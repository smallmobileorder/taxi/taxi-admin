import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, forkJoin, Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../services/auth.service';
import {OrderService} from '../services/order.service';
import {StateOrder} from '../models/OrderActive';

@Injectable({
  providedIn: 'root'
})
export class OrderResolver implements Resolve<any> {

  constructor(private orderService: OrderService, private authService: AuthService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    const id = route.paramMap.get('id');
    return this.orderService.getOrderObservable(id).pipe(catchError(() => {
        this.authService.refresh();
        return EMPTY;
      }));
  }
}

