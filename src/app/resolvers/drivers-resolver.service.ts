import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, Observable} from 'rxjs';
import {DriversService} from '../services/drivers.service';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class DriversResolver implements Resolve<any> {

  constructor(private driverService: DriversService, private authService: AuthService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

      return this.driverService.getDriversObservable('', 'person.id', 'desc', '15',
        '0').pipe(catchError(() => {
      this.authService.refresh();
      return EMPTY;
    }));
  }
}

