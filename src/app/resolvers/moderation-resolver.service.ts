import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {EMPTY, Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../services/auth.service';
import {AccountApplicationService} from '../services/account-application.service';

@Injectable({
  providedIn: 'root'
})
export class ModerationResolver implements Resolve<any> {

  constructor(private accountApplicationService: AccountApplicationService, private authService: AuthService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.accountApplicationService.getModerationListObservable('', 'person.id', 'DESC', '15',
      '0').pipe(catchError(() => {
      this.authService.refresh();
      return EMPTY;
    }));
  }
}

