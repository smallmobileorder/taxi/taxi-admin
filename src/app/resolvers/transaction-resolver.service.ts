import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../services/auth.service';
import {TransactionsService} from '../services/transactions.service';

@Injectable({
  providedIn: 'root'
})
export class TransactionsResolver implements Resolve<any> {

  constructor(private transactionsService: TransactionsService, private authService: AuthService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

    return this.transactionsService.getTransactionObservable('', 'timestamp', 'desc', '10',
      '0').pipe(catchError(() => {
      this.authService.refresh();
      return EMPTY;
    }));
  }
}

