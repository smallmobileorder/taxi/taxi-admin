import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {EMPTY, Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../services/auth.service';
import {ConfigService} from '../services/config.service';

@Injectable({
  providedIn: 'root'
})
export class MainConfigResolver implements Resolve<any> {

  constructor(private configService: ConfigService, private authService: AuthService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.configService.getMainConfigObservable().pipe(
      catchError(() => {
        this.authService.refresh();
        return EMPTY;
      }));
  }
}

