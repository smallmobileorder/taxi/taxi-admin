import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {EMPTY, forkJoin, Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../services/auth.service';
import {OrderService} from '../services/order.service';
import {StateOrder} from '../models/OrderActive';

@Injectable({
  providedIn: 'root'
})
export class ActiveOrdersResolver implements Resolve<any> {

  constructor(private orderService: OrderService, private authService: AuthService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return forkJoin(this.orderService.getOrdersObservable('', 'orders.date_create', 'desc', '15',
      '0', StateOrder.FINDING).pipe(catchError(() => {
        this.authService.refresh();
        return EMPTY;
      })),
      this.orderService.getOrdersObservable('', 'orders.date_create', 'desc', '15',
        '0', StateOrder.WAITING).pipe(catchError(() => {
        this.authService.refresh();
        return EMPTY;
      })),
      this.orderService.getOrdersObservable('', 'orders.date_create', 'desc', '15',
        '0', StateOrder.TRIP).pipe(catchError(() => {
        this.authService.refresh();
        return EMPTY;
      }))
    );
  }
}

