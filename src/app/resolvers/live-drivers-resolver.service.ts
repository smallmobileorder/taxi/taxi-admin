import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {EMPTY, Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthService} from '../services/auth.service';
import {LiveDriversService} from '../services/live-drivers.service';
import {DriversService} from '../services/drivers.service';

@Injectable({
  providedIn: 'root'
})
export class LiveDriversResolver implements Resolve<any> {

  constructor(private liveDriversService: LiveDriversService, private authService: AuthService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.liveDriversService.getDriverLatLngObservable().pipe(catchError(() => {
      this.authService.refresh();
      return EMPTY;
    }));
  }
}

