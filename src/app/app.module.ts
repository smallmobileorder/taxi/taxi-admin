import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app.router';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './components/login/login.component';
import {DemoMaterialModule} from './material-module';
import {MainComponent} from './components/main/main.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatMenuModule, MatButtonModule, MatIconModule, MatCardModule, MatSidenavModule} from '@angular/material';
import {DriversComponent} from './components/drivers/drivers.component';
import {ClientsComponent} from './components/clients/clients.component';
import {TransactionsComponent} from './components/transactions/transactions.component';
import {ModerationComponent} from './components/moderation/moderation.component';
import {ActiveOrderComponent} from './components/active-order/active-order.component';
import {CompleteOrderComponent} from './components/complete-order/complete-order.component';
import {MainConfigComponent} from './components/main-config/main-config.component';
import {
  DialogOverviewExampleDialogComponent, RegistrationDriverComponent
} from './components/registration-internal/registration-driver.component';
import {DriversService} from './services/drivers.service';
import {AuthService} from './services/auth.service';
import {AuthInterceptors} from './services/auth.interceptors';
import {SupportComponent} from './components/support/support.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {
  DialogApproveDialogComponent,
  ModerationApproveComponent
} from './components/moderation-approve/moderation-approve.component';
import {BadRequestComponent} from './components/bad-request/bad-request.component';
import {AddTransactionComponent} from './components/add-transaction/add-transaction.component';
import {OrderInfoComponent} from './components/order-info/order-info.component';
import {TariffsConfigComponent} from './components/tariffs-config/tariffs-config.component';
import {BottomSheetComponent, LiveDriversComponent} from './components/live-drivers/live-drivers.component';
import {AgmCoreModule} from '@agm/core';
import {AgmOverlays} from 'agm-overlays';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    DriversComponent,
    ClientsComponent,
    TransactionsComponent,
    ModerationComponent,
    ActiveOrderComponent,
    CompleteOrderComponent,
    MainConfigComponent,
    RegistrationDriverComponent,
    DialogOverviewExampleDialogComponent,
    DialogApproveDialogComponent,
    SupportComponent,
    ModerationApproveComponent,
    BadRequestComponent,
    AddTransactionComponent,
    OrderInfoComponent,
    TariffsConfigComponent,
    LiveDriversComponent,
    BottomSheetComponent
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB4ljXe2eL7ZkbIcTivSpNPBr4zcauESlE'
    }),
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    DemoMaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatSidenavModule,
    NgxMaterialTimepickerModule,
    AgmOverlays
  ],
  entryComponents: [DialogOverviewExampleDialogComponent, DialogApproveDialogComponent,     BottomSheetComponent],
  providers: [DriversService, AuthService, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptors,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
