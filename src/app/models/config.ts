export interface Config {
  min_balance_driver: string;
  max_count_halt: string;
  time_free_waiting: string;
  time_for_cancel_order: string;
  price_for_min_waiting: string;
  price_for_halt: string;
  price_for_halt_in_way: string;
  time_for_cancel_order_without_driver: string;
  time_for_finish_order: string;
  time_for_finish_order_without_rating: string;
  life_time_rates: string;
  min_balance_driver_for_accept_order: string;
  time_for_check_status_call_client: string;
  interval_for_check_status_call_client: string;
  count_for_check_status_call_client: string;
  cancel_order_without_call: string;
  call_for_up_price: string;
  commission_for_fine: string;
  amount_fine: string;
  id_city: number;
}

export interface DriversTariffsConfig {
  id: string;
  name_tariff: string;
  min_price: string;
  price_for_meter: string;
  price_for_minute: string;
  city_id: string;
}

export interface DriversTariffsConfigs {
  tariffs: DriversTariffsConfig[];
}
