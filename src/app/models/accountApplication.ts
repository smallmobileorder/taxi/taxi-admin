export interface AccountApplication {
  id: number;
  first_name: string;
  second_name: string;
  patronymic: string;
  gender: boolean;
  date_birthday: string;
  address1: string;
  address2: string;
  phone: string;
  email: string;
  seria_pasport: string;
  number_pasport: string;
  date_vidachi_pasport: string;
  seria_voditelskoye: string;
  number_voditelskoye: string;
  date_vidachi_voditelskoye: string;
  date_access_voditelskoye: string;
  mark: string;
  model: string;
  color: string;
  number: string;
  year_making: string;
  count_of_doors: string;
  vin: string;
  comment: string;
  status: string;
  date_writing: string;
  date_update: string;
}

export interface AccountApplicationShort {
  id: number;
  fio: string;
  phone: string;
  status: number;
  date_writing: string;
  date_update: string;
  status_text: string;
}

export interface AccountApplicationShortTable {
  accounts: AccountApplicationShort[];
  length: number;
}

export interface RequestRegister {
  model_name: string;
  car_number: string;
  mark_name: string;
  vin_value: string;
  color: string;
  year_made: string;
  photo_register_back: string;
  photo_register_front: string;
  count_doors: string;
  license_front: string;
  license_back: string;
  serial_license: string;
  number_license: string;
  date_register_license: string;
  date_end_license: string;
  address_value_fact: string;
  address_value_register: string;
  serial_password: string;
  number_password: string;
  date_register_password: string;
  photo_passport_main: string;
  photo_passport_register: string;
  first_name: string;
  last_name: string;
  birthday_value: string;
  gender: boolean;
  patronymic: string;
  phone_number: string;
  balance: string;
  comment: string;
  data_status_change: string;
  data_create: string;
  state: string;
}
