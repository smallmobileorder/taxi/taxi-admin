export interface Driver {
  name: string;
  id: number;
  phone: string;
  tariff: string;
  balance: number;
  auto: string;
  isBlock: boolean;
}
export interface DriverTable {
  drivers: Driver[];
  length: number;
}

export interface DriverLatLng {
  id: string;
  lat: number;
  lng: number;
  last_angel: number;
  phone_number: number;
  first_name: number;
  last_name: number;
  patronymic: number;
}
