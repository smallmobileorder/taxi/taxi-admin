export interface Transaction {
  date: string;
  id: number;
  phone: string;
  balance_before: number;
  sum: number;
  balance_after: number;
  type_operation: string;
  payment_system: string;
  type_object: string;
  type_balance: string;
  comment: string;
}

export interface TransactionTable {
  transactions: Transaction[];
  length: number;
}


export enum TYPE_OPERATION {
  INTERNAL = 'INTERNAL',
  COMMISSION = 'COMMISSION',
  PAY_ORDER = 'PAY_ORDER',
  ADD_BALANCE = 'ADD_BALANCE'
}

export enum TYPE_OBJECT {
  ORDER = 'ORDER',
  WALLET = 'WALLET'
}

export enum TYPE_BALANCE {
  BALANCE_DRIVER = 'BALANCE_DRIVER',
  BALANCE_ORGANIZATION = 'BALANCE_ORGANIZATION'
}

export enum TYPE_PUSH {
  ALL = 'ALL', CLIENTS = 'CLIENTS', DRIVERS = 'DRIVERS', PERSONAL = 'PERSONAL'
}
