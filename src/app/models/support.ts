export interface Support {
  id: string;
  date: string;
  email: string;
  message: string;
}
