
export interface Client {
  name: string;
  id: number;
  phone: string;
  isBlock: boolean;
}

export interface ClientTable {
  clients: Client[];
  length: number;
}
