export interface OrderActive {
  number_order: string;
  timer: string;
  addressDepart: string;
  addressDest: string;
  driver: string;
  phone: string;
  phoneDriver: string;
  client: string;
  state: StateOrder;
}

export interface OrderActiveTable {
  ordersFinding: OrderActive[];
  ordersWaiting: OrderActive[];
  ordersTrip: OrderActive[];
}

export interface OrderFinish {
  number_order: string;
  date_finish: string;
  date_start: string;
  addressDepart: string;
  addressDest: string;
  driver: string;
  client: string;
  phone: string;
  phoneDriver: string;
  sum: string;
  status: Status;
}

export interface OrderFinishTable {
  orders: OrderFinish[];
  length: number;
}

export enum Status {
  SUCCESS = 'Заказ успешно завершен',
  CANCELED_CLIENT = 'Отменен клиентом',
  CANCELED_DRIVER = 'Отменен водителем'
}

export enum StateOrder {
  PRELIMINARY = 'PRELIMINARY',
  FINDING = 'FINDING',
  WAITING = 'WAITING',
  TRIP = 'TRIP',
  FINISHED = 'FINISHED'
}

export enum STATUS_ORDER {
  SUCCESS = 'SUCCESS', DIFFERENT_REASON = 'DIFFERENT_REASON', DRIVER_CANCELED = 'DRIVER_CANCELED', CLIENT_CANCELED = 'CLIENT_CANCELED'
}

export interface OrderInfo {
  id_order: string;
  state_order: StateOrder;
  datetime_start: string;
  datetime_finish: string;
  phone_client: string;
  phone_driver: string;
  fio_client: string;
  fio_driver: string;
  address_depart: string;
  address_dest: string;
  sum: string;
}
